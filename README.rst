- http://code.google.com/p/crypto-js/
- http://rechten.uvt.nl/koops/cryptolaw/cls2.htm
- http://www.bis.doc.gov/encryption/default.htm
- http://www.tml.tkk.fi/Studies/Tik-110.300/1998/Essays/restrictions.html
- http://home.versatel.nl/MAvanEverdingen/Code/
- http://gilc.org/crypto/crypto-survey.html
- http://www.cryptolaw.org/
- http://torrentfreak.com/how-to-stop-domain-names-being-seized-by-the-us-government-110205/

Table 1: Brute force attack times
Key length  Time to search the keyspace
Symmetric (RC4/AES/SRP/TFS) Asymmetric (RSA)    High-end computer   University network  Military
40 bits, 7 chars    274 bits, 46 chars  weeks   hours   microseconds
64 bits, 11 chars   512 bits, 86 chars  millennia   decades minutes
80 bits, 14 chars   1024 bits, 171 chars    infinite    infinite    centuries
128 bits, 22 chars  3072 bits, 512 chars    infinite    infinite    millennia
