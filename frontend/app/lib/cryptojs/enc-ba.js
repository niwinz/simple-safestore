(function () {
    var C = CryptoJS;
    var C_lib = C.lib;
    var WordArray = C_lib.WordArray;
    var C_enc = C.enc;

    var ByteArray = C_enc.ByteArray = {
        stringify: function (wordArray) {
            var words = wordArray.words;
            var sigBytes = wordArray.sigBytes;

            var realSize = sigBytes/4;

            var buffer = new ArrayBuffer(realSize);
            var view = new Uint8Array(buffer);

            for(var i=0; i<realSize; i++) {
                view[i] = words[i];
            }

            return view;
        },
        parse: function (buffer) {
            return WordArray.create(buffer);
        }
    };
}());
