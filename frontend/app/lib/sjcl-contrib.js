(function() {
    var bitLength = function(view) {
        return view.byteLength * 8;
    };

    var xor = function(x,y) {
        return [x[0]^y[0],x[1]^y[1],x[2]^y[2],x[3]^y[3]];
    };

    sjcl.mode.cbc2k = {
        name: "cbc2k",

        encrypt: function(prp, data, iv, adata) {
            if (adata && adata.length) {
                throw new sjcl.exception.invalid("cbc can't authenticate data");
            }

            if (bitLength(iv) !== 128) {
                throw new sjcl.exception.invalid("cbc iv must be 128 bits");
            }

            var view = new Int32Array(data);

            var missingLength = 4 - (view.length % 4);
            var bl = bitLength(data);
            var bp = 0, i;

            var finalLength = (view.length + missingLength) * 4

            var outputBuffer = new ArrayBuffer(finalLength);
            var outputView = new Int32Array(outputBuffer);

            if (bl&7) {
                throw new sjcl.exception.invalid("pkcs#5 padding only works for multiples of a byte");
            }

            /* Encrypt a non-final block */
            for (i=0; bp+128 <= bl; i+=4, bp+=128) {
                var tmp = xor(iv, view.subarray(i, i+4))
                iv = prp.encrypt(tmp);

                outputView[i]   = iv[0];
                outputView[i+1] = iv[1];
                outputView[i+2] = iv[2];
                outputView[i+3] = iv[3];
            }

            /* Construct the pad. */
            bl = (16 - ((bl >> 3) & 15)) * 0x1010101;

            var rest = _.map(view.subarray(i, i+4), function(x) { return x; });
            for(var x=0; x<missingLength; x++) rest.push(bl);

            iv = prp.encrypt(xor(iv, rest));
            for(var x=i, y=0; y<iv.length; x++, y++) {
                outputView[x] = iv[y];
            }

            return outputBuffer;
        },

        decrypt: function(prp, cipherdata, iv, adata) {
            if (adata && adata.length) {
                throw new sjcl.exception.invalid("cbc can't authenticate data");
            }

            if (bitLength(iv) !== 128) {
                throw new sjcl.exception.invalid("cbc iv must be 128 bits");
            }

            if (bitLength(cipherdata) & 127) {
                throw new sjcl.exception.corrupt("cbc ciphertext must be a positive multiple of the block size");
            }

            var i, bi, bo;

            var tmpView = new Int32Array(new ArrayBuffer(cipherdata.byteLength));
            var dataView = new Int32Array(cipherdata);

            for (i=0; i<dataView.length; i+=4) {
                bi = dataView.subarray(i,i+4);
                bo = xor(iv, prp.decrypt(bi));

                tmpView[i]   = bo[0];
                tmpView[i+1] = bo[1];
                tmpView[i+2] = bo[2];
                tmpView[i+3] = bo[3];
                iv = bi;
            }

            /* check and remove the pad */
            bi = tmpView[i-1] & 255;
            if (bi == 0 || bi > 16) {
                throw new sjcl.exception.corrupt("pkcs#5 padding corrupt");
            }

            //return tmpView.subarray(0, bi/4);
            return tmpView.buffer.slice(0, tmpView.buffer.byteLength-bi);
            //return tmpView.subarray(0, (bi*8)/32);
            //return w.bitSlice(output, 0, output.length*32 - bi*8);
        }
    };
}).call(this);
