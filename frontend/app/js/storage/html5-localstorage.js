'use strict';


(function() {
    var backend = window.localStorage;
    var setItem = function(key, value) {
        if (value === undefined) {
            backend.removeItem(key);
        } else {
            backend.setItem(key, JSON.stringify(value));
        }
    };

    var getItem = function(key) {
        var data = backend.getItem(key);
        if (data === null) {
            return undefined;
        } else {
            return JSON.parse(data);
        }
    };


    if (this.safestore === undefined) this.safestore = {};
    if (this.safestore.storage === undefined) this.safestore.storage = {};

    var Service = function() {};
    Service.prototype.set = function(key, value, callback) {
        if (key === undefined) return;

        if (_.isObject(key)) {
             _.each(key, function(value, key) { setItem(key, value); });
        } else {
            setItem(key, value);
        }

        if (_.isFunction(value)) {
            callback = value;
        }

        if (callback !== undefined) {
            callback.apply(this);
        }
    };

    Service.prototype.get = function(key, callback) {
        var returnValue;

        if (_.isArray(key)) {
            var result = {};
            _.each(key, function(kitem) {
                result[kitem] = getItem(kitem);
            });

            returnValue = result;
        } else {
            returnValue = getItem(key);
        }

        if (callback !== undefined) {
            callback.apply(this, [returnValue]);
        }
    };

    Service.prototype.clear = function(callback) {
        backend.clear();
        if (callback !== undefined && _.isFunction(callback)) {
            callback.apply(this);
        }
    };

    this.safestore.storage.backend = Service;
}).call(this);
