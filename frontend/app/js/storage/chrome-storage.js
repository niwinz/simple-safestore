'use strict';

angular.module('myApp.storage', [], function($provide) {
    $provide.factory('$store', ['$window', '$crypto', function($window, $crypto) {
        var backend = chrome.storage.local;

        var initialize = function(callback) {
            var self = this;
            this.data = null;

            backend.get(["pubkey", "token", "uuid", "privkey", "passphrase"], function(data) {
                self.data = data;

                if (self.data.pubkey !== undefined) {
                    self.keys.pubKey = $crypto.parsePublicKey(this.data.pubkey);
                }

                if (self.data.privkey) {
                    var privKey = $crypto.parsePrivateKey(self.data.privkey, self.data.passphrase);
                    if (privKey.is_valid()) {
                        self.keys.privKey = privKey;
                        _.defer(callback, true)
                    } else {
                        _.defer(callback, false)
                    }
                }
                _.defer(callback, true)
            });
        };

        var service = {
            "set": function(key, value, callback) {
                if (_.isObject(key)) {
                    _.each(key, function(value, key) { setItem(key, value); });
                } else {
                    setItem(key, value);
                }

                if (_.isFunction(value)) {
                    callback = value;
                }

                if (callback !== undefined) {
                    callback();
                }
            },

            "get": function(key, callback) {
                var returnValue;

                if (_.isArray(key)) {
                    var result = {};
                    _.each(key, function(kitem) {
                        result[kitem] = getItem(kitem);
                    });

                    returnValue = result;
                } else {
                    returnValue = getItem(key);
                }

                if (callback !== undefined) {
                    callback(returnValue);
                }
            },

            "remove": function(callback) {
                backend.clear();
            },

            "data": {},
            "keys": {},
            "initializeKeys": initialize,
        };

        _.bindAll(service);
        return service;
    }]);

}).value('version', '0.1');
