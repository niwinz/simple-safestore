'use strict';

/* Services */

angular.module('safestore.services', ['safestore.config'], function($provide) {
    $provide.factory('$notify', ['$rootScope', function($rootScope) {
        return function(type, messages, timeout) {
            $rootScope.$broadcast('$notify', type, messages, timeout);
        };
    }]);

    $provide.factory("$urls", ['safestore.config', function(config) {
        var urls = {
            "auth": "/api/v1/login",
            "blobs": "/api/v1/blobs",
            "blob": "/api/v1/blobs/%s",
            "blob-share": "/api/v1/blobs/%s/shares",
            "user": "/api/v1/user",
            "contacts": "/api/v1/contacts",
            "contact": "/api/v1/contacts/%s",
            "verify": "/api/v1/user/activation",
            "profile": "/api/v1/profile",
            "messages": "/api/v1/messages",
            "message": "/api/v1/messages/%s"
        }, host = config.host, scheme=config.scheme;

        var UrlManager = function() {
            this.body = $("body");
            this.initialized = false;
        };

        UrlManager.prototype.initialize = function() {
            if (this.initialized) return;
            this.initialized = true;
        };

        UrlManager.prototype.get = function() {
            this.initialize();

            var args = _.toArray(arguments);
            var name = args.slice(0, 1);
            var params = [urls[name]];

            _.each(args.slice(1), function(item) {
                params.push(item);
            });

            var url = _.str.sprintf.apply(null, params);
            return _.str.sprintf("%s://%s%s", scheme, host, url);
        };
        return new UrlManager();
    }]);

    $provide.factory('$crypto', function() {
        return safestore.crypt;
    });

    $provide.factory('loader', ['$store', '$urls', function($store, $urls) {
        var service = {};

        var launchWorker = function(path, data) {
            var deferred = Q.defer();
            var worker = new Worker(path);

            worker.addEventListener('message', function(e) {
                if (e.data.event === "finish") {
                    deferred.resolve(e.data.data);
                    worker.terminate();
                } else {
                    deferred.notify(e.data.data);
                }
            });

            worker.addEventListener('error', function(e) {
                deferred.reject({
                    lineno: e.lineno,
                    filename: e.filename,
                    message: e.message
                });
                worker.terminate();
            });

            worker.postMessage(data);
            return deferred.promise;
        };

        service.loadNotes = function() {
            var data = _.extend({}, $store.data, {
                "url": $urls.get('blobs')
            });

            return launchWorker("js/workers/load-notes.js", data);
        };

        service.loadContacts = function() {
            var data = _.extend({}, $store.data, {
                "url": $urls.get('contacts')
            });

            return launchWorker("js/workers/load-contacts.js", data);
        }

        return service;
    }]);
}).value('version', '0.1');
