"use strict";

(function() {
    var kwords = ["username", "pubkey", "token", "id", "privkey", "passphrase"];
    var backend = new safestore.storage.backend();

    var data = {};
    var keys = {};
    var cache = {};

    var initializeKeys = function() {
        var defer = Q.defer();

        backend.get(kwords, function(dt) {
            data = _.extend(data, dt);

            if (data.pubkey !== undefined) {
                keys.pubKey = safestore.crypt.parsePublicKey(data.pubkey);
            }

            if (data.privkey) {
                keys.privKey = safestore.crypt.parsePrivateKey(data.privkey);
            }

            if (keys.privkey !== undefined) {
                var encryptedTest = self.keys.pubKey.encrypt("???");
                var decryptedTest = self.keys.privKey.decrypt(encryptedTest);
                if (decryptedTest !== "???") {
                    keys.privKey = undefined;
                    data.privkey = undefined;
                    backend.set("privkey", undefined);
                    deferred.reject("INVALID KEY");
                } else {
                    deferred.resolve("KEY OK");
                }
            } else {
                defer.reject("NO KEY FOUND");
            }
        });

        return defer.promise;
    };

    safestore.storage.data = data;
    safestore.storage.keys = keys;
    safestore.storage.cache = cache;
    safestore.storage.initializeKeys = initializeKeys;
    safestore.storage.instance = backend;

    angular.module('safestore.services.storage', ['safestore.config'], function($provide) {
        $provide.factory('$store', [function() {
            return safestore.storage;
        }]);
    });
}).call(this);
