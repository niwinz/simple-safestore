'use strict';

/* Services */

(function() {
    /* Web Workers abstraction layer. */
    var WebWorker = function(name) {
        this.name = name;
        this.callback = function() {};

        _.bindAll(this);
    };

    WebWorker.prototype.onmessage = function(callback) {
        this.callback = callback;
    };

    WebWorker.prototype.start = function(data) {
        var self = this;

        this.worker = new Worker(this.name);
        this.worker.addEventListener('message', function(e) {
            var result = {
                event: e.data.event || "unknown",
                data: e.data.data || {}
            };
            self.callback.call(this, result);
        }, false);

        this.worker.addEventListener('error', function(e) {
            var result = {
                event: "error",
                data: {
                    lineno: e.lineno,
                    filename: e.filename,
                    message: e.message
                }
            };
            self.callback.call(this, result);
        }, false);

        this.worker.postMessage(data);
    };

    WebWorker.prototype.stop = function() {
        if (this.worker !== undefined) {
            this.worker.terminate();
            this.worker = undefined;
        }
    };

    var Upload = function() {
        var self = this;
        var defered = new Q.defer();

        this.worker = new WebWorker("js/workers/upload.js");

        this.worker.onmessage(function(data) {

            console.log("EVENT", data.event, data.data);

            switch(data.event) {
                case "progress":
                    defered.notify(data.data);
                    break;

                case "finish":
                    defered.resolve(data.data);
                    break;

                case "error":
                    defered.reject(data.data);
                    break;
            };
        });

        this.q = defered.promise.then(function(data) {
            console.log("success", data);
        }, function(data) {
            console.log("error", data);
        }, function(data) {
            console.log("progress", data);
        });
    };

    Upload.prototype.start = function(file) {
        this.worker.start(file);
    };

    var service = {};
    service.Upload = Upload;

    angular.module('safestore.uploads', ['safestore.config'], function($provide) {
        $provide.factory('$fupload', ['$rootScope', function($rootScope) {
            return service;
        }]);
    });

    if (this.safestore === undefined) {
        this.safestore = {};
    }

    this.safestore.files = service;
}).call(this);
