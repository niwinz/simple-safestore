'use strict';

(function() {
    var safestoreConfig = function($routeProvider, $locationProvider, $httpProvider, $provide, $compileProvider) {
        $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: LoginController});
        $routeProvider.when('/register', {templateUrl: 'partials/register.html', controller: RegisterController});
        $routeProvider.when('/verify', {templateUrl: 'partials/verify.html', controller: VerifyController});
        $routeProvider.when('/notes', {templateUrl: 'partials/notes.html', controller: NotesController});
        $routeProvider.when('/privkey', {templateUrl: 'partials/privkey.html', controller: PrivkeyController});
        $routeProvider.when('/profile', {templateUrl: 'partials/profile.html', controller: ProfileController});
        $routeProvider.when('/messages', {templateUrl: 'partials/messages.html', controller: MessagesController});
        $routeProvider.when('/shares', {templateUrl: 'partials/pending-shares.html',
                                            controller: PendingSharesController});
        $routeProvider.otherwise({redirectTo: '/login'});
        $locationProvider.hashPrefix('!');

        $httpProvider.defaults.headers.delete = {"Content-Type": "application/json"};
        $httpProvider.defaults.headers.patch = {"Content-Type": "application/json"};

        $provide.factory("authHttpIntercept", ["$q", "$location", function($q, $location) {
            return function(promise) {
                return promise.then(null, function(response) {
                    if (response.status === 401) {
                        $location.url("/login");
                    }
                    return $q.reject(response);
                });
            };
        }]);

        $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|blob):/);
        $httpProvider.responseInterceptors.push('authHttpIntercept');
    };

    var safestoreRun = function($store, $rootScope) {
        $store.initializeKeys()
            .then(function(msg) {
                console.log(msg);
            }, function(msg) {
                console.log(msg);
            }).finally(function() {
                $rootScope.auth = {
                    "id": $store.data.id,
                    "username": $store.data.username
                };
            });
    };

    angular.module('safestore', ['safestore.filters', 'safestore.services', 'safestore.directives', 'safestore.uploads', 'safestore.services.storage'])
        .config(['$routeProvider', '$locationProvider', '$httpProvider', '$provide', '$compileProvider', safestoreConfig])
        .run(['$store', '$rootScope', safestoreRun]);

    //window.onbeforeunload = function() {
    //    return "Really want to reload the page? That action can interrupt uploads and other processes!";
    //}

}).call(this);
