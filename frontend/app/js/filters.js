'use strict';

/* Filters */

angular.module('safestore.filters', []).
    filter('interpolate', ['version', function(version) {
        return function(text) {
            return String(text).replace(/\%VERSION\%/mg, version);
        }
    }]).
    filter('truncate', function() {
        return function(input, num) {
            if (num === undefined) num =  25;
            return _.str.prune(input, num);
        };
    }).
    filter('timesince', function() {
        return function(input) {
            return moment(input).fromNow();
        };
    }).
    filter('trim', function() {
        return function(input) {
            return _.str.trim(input);
        };
    });
