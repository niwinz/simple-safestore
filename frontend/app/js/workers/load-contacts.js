importScripts('/lib/underscore.js');
importScripts('/lib/min/pidcrypt.min.js');
importScripts('/lib/min/sjcl.min.js');
importScripts('/lib/sjcl-contrib.js');
importScripts('/lib/q.js');
importScripts('/js/utils.js');


self.addEventListener("message", function(e) {
    var urlopen = function(data, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                callback(JSON.parse(xhr.responseText));
            }
        };

        xhr.open('GET', data.url, true); // Note: synchronous
        xhr.setRequestHeader("X-SAFESTORE-TOKEN", data.token);
        xhr.send();
    };

    urlopen(e.data, function(data) {
        var privkey;

        if (e.data.privkey) {
            privkey = safestore.crypt.parsePrivateKey(e.data.privkey);
        }

        var translate = function(item) {
            if (privkey === undefined) {
                return item;
            }

            var key = privkey.decrypt(item.meta);
            item.alias = safestore.crypt.decrypt(key, item.alias);
            item.translated = true;
            return item;
        };

        _.each(data, function(item) {
            postMessage({event:"progress", data: translate(item)});
        });

        postMessage({event:"finish", data:null});
    });
}, false);
