importScripts('/lib/underscore.js');
importScripts('/lib/min/pidcrypt.min.js');
importScripts('/lib/min/sjcl.min.js');
importScripts('/lib/sjcl-contrib.js');
importScripts('/lib/q.js');
importScripts('/js/utils.js');



self.addEventListener('message', function(e) {
    var file = e.data;
    var reader = new FileReader();

    var updateProgress = function(e) {
        if (e.lengthComputable) {
            var data = {loaded: e.loaded, total: e.total};
            self.postMessage({event:"progress", data: data});
        }
    };

    reader.onload = function(event) {
        var xhr = new XMLHttpRequest();
        var buffer = event.target.result;
        var view = new Uint8Array(buffer);


        xhr.upload.addEventListener("progress", updateProgress, false);
        xhr.onreadystatechange = function() {
            self.postMessage({event:"state change", data: {'state':xhr.readyState}});

            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    postMessage({event:"finish", data:xhr.response});
                }
                else {
                    postMessage({event: "error", data: xhr.response});
                }
            }
        };

        xhr.open("POST", "http://localhost:8000/tests/binary-upload", true);
        xhr.overrideMimeType(file.type);
        xhr.setRequestHeader("X_FILENAME", file.name);
        xhr.send(view);

        // Encrypt
        //self.postMessage({event:"timer 1", data: {'time': new Date()}});
        //var wordArray = CryptoJS.lib.WordArray.create(view);
        //self.postMessage({event:"timer 2", data: {'time': new Date()}});
        //var encryptedData = CryptoJS.AES.encrypt(wordArray, "sample-password");
        //self.postMessage({event:"timer 3", data: {'time': new Date()}});
        //var stringData = encryptedData.toString();
        //self.postMessage({event:"timer 4", data: {'time': new Date()}});
        //xhr.send(stringData);
    };

    reader.readAsArrayBuffer(file);
}, false);
