"use strict";

(function() {
    var certParser = function(cert){
        var lines = cert.split('\n');
        var read = false;
        var b64 = false;
        var end = false;
        var flag = '';
        var retObj = {};
        retObj.info = '';
        retObj.salt = '';
        retObj.iv;
        retObj.b64 = '';
        retObj.aes = false;
        retObj.mode = '';
        retObj.bits = 0;

        for(var i=0; i< lines.length; i++){
            flag = lines[i].substr(0,9);
            if(i==1 && flag != 'Proc-Type' && flag.indexOf('M') == 0)//unencrypted cert?
                b64 = true;

            switch(flag) {
                case '-----BEGI':
                    read = true;
                    break;
                case 'Proc-Type':
                    if(read) retObj.info = lines[i];
                    break;
                case 'DEK-Info:':
                    if(read) {
                        var tmp = lines[i].split(',');
                        var dek = tmp[0].split(': ');
                        var aes = dek[1].split('-');
                        retObj.aes = (aes[0] == 'AES')?true:false;
                        retObj.mode = aes[2];
                        retObj.bits = parseInt(aes[1]);
                        retObj.salt = tmp[1].substr(0,16);
                        retObj.iv = tmp[1];
                    }
                    break;
                case '':
                    if(read) b64 = true;
                    break;
                case '-----END ':
                    if(read) {
                        b64 = false;
                        read = false;
                    }
                    break;
                default:
                    if(read && b64) retObj.b64 += pidCryptUtil.stripLineFeeds(lines[i]);
            }
        }
        return retObj;
    };

    /* Private Key definition */

    var RSAPrivKey = function(rawKey, passphrase) {
        _.bindAll(this);

        this.rsa = new pidCrypt.RSA();

        var pem = certParser(rawKey);
        var asn;

        if (pem.aes === true) {
            var aes = new pidCrypt.AES.CBC();
            var k_and_iv = aes.createKeyAndIv({password: passphrase,
                                               salt: pem.salt.toLowerCase(),
                                               bits: parseInt(pem.bits)});

            aes.initByValues(pem.b64, k_and_iv.key, pem.iv.toLowerCase(),
                    {UTF8: false,A0_PAD: false,nBits:parseInt(pem.bits)});

            asn = pidCrypt.ASN1.decode(pidCryptUtil.toByteArray(aes.decrypt()));
        } else {
            var key = pidCryptUtil.decodeBase64(pem.b64);
            asn = pidCrypt.ASN1.decode(pidCryptUtil.toByteArray(key));
        }
        var tree = asn.toHexTree();
        this.rsa.setPrivateKeyFromASN(tree);
    };

    RSAPrivKey.prototype.decrypt = function(data) {
        var crypted = pidCryptUtil.decodeBase64(pidCryptUtil.stripLineFeeds(data));
        return this.rsa.decrypt(pidCryptUtil.convertToHex(crypted));
    };

    RSAPrivKey.prototype.is_valid = function() {
        return this.rsa.coeff !== null;
    };

    /* Public Key definition */

    var RSAPubKey = function(rawKey) {
        _.bindAll(this);
        var params = certParser(rawKey);
        var key = pidCryptUtil.decodeBase64(params.b64);

        this.rsa = new pidCrypt.RSA();

        var asn = pidCrypt.ASN1.decode(pidCryptUtil.toByteArray(key));
        var tree = asn.toHexTree();
        this.rsa.setPublicKeyFromASN(tree);
    };

    RSAPubKey.prototype.encrypt = function(data) {
        var crypted = this.rsa.encrypt(data);
        return pidCryptUtil.encodeBase64(pidCryptUtil.convertFromHex(crypted));
    };

    /* Cipher options */
    var sjcl_bufferDefaults = { v:1, iter:1000, ks:256, ts:64, mode:"cbc2k", adata:"", cipher:"aes" };
    var sjcl_textDefaults = {ks:256};

    /* Cipher namespace */

    var crypt = {};
    var utils = {};

    crypt.RSAPubKey = RSAPubKey;
    crypt.RSAPrivKey = RSAPrivKey;
    crypt.certParser = certParser;

    crypt.parsePublicKey = function(keydata) {
        return new RSAPubKey(keydata);
    };

    crypt.parsePrivateKey = function(keydata, passphrase) {
        return new RSAPrivKey(keydata, passphrase);
    };

    crypt.randomKey = function(length) {
        length = length || 512;

        var buffer = new ArrayBuffer(length/8);
        var view = new Uint8Array(buffer);
        crypto.getRandomValues(view);
        return utils.ba2hex(view);
    };

    crypt.randomIv = function() {
        var buffer = new ArrayBuffer(16);
        var view = new Int32Array(buffer);
        crypto.getRandomValues(view);
        return view;
    };

    crypt.randomSalt = function() {
        var buffer = new ArrayBuffer(8);
        var view = new Int32Array(buffer);
        crypto.getRandomValues(view);
        return _.map(view, function(item) { return item; });
    };

    crypt.encryptBuffer = function(password, buffer) {
        var p = _.extend({}, {iv: crypt.randomIv()}, sjcl_bufferDefaults);

        p.salt = crypt.randomSalt();
        password = sjcl.misc.pbkdf2(password, p.salt);

        var cipher = new sjcl.cipher[p.cipher](password);
        p.ct = sjcl.mode[p.mode].encrypt(cipher, buffer, p.iv, "", p.ts);
        return p;
    };

    crypt.decryptBuffer = function(password, data) {
        var p = _.extend({}, sjcl_bufferDefaults, data);
        password = sjcl.misc.pbkdf2(password, p.salt);

        var cipher = new sjcl.cipher[p.cipher](password);
        var buffer = sjcl.mode[p.mode].decrypt(cipher, p.ct, p.iv, "", p.ts);
        return buffer;
    };

    crypt.encryptText = function(password, data) {
        return sjcl.encrypt(password, data, sjcl_textDefaults);
    };

    crypt.decryptText = function(password, data) {
        return sjcl.decrypt(password, data, sjcl_textDefaults);
    };

    // Aliases
    crypt.encrypt = crypt.encryptText;
    crypt.decrypt = crypt.decryptText;

    utils.ab2str = function(buff) {
        return String.fromCharCode.apply(null, new Uint16Array(buff));
    };

    utils.ab2bytes = function(buffer) {
        return String.fromCharCode.apply(null, new Uint8Array(buffer));
    };

    utils.str2ab = function(str) {
        var buffer = new ArrayBuffer(str.length*2); // 2 bytes for each char
        var view = new Uint16Array(buffer);
        for (var i=0, strLen=str.length; i<strLen; i++) {
            view[i] = str.charCodeAt(i);
        }
        return buffer;
    };

    utils.bytes2ab = function(bytes) {
        var buffer = new ArrayBuffer(bytes.length);
        var view = new Uint8Array(buffer);

        for(var i=0; i < bytes.length; i++) {
            view[i] = bytes.charCodeAt(i);
        }

        return buffer;
    };

    utils.ba2hex = function(data) {
        var _data = _.map(data, function(c) {
            var _hex = c.toString(16);
            return _hex.length >1 ? _hex : '0' + _hex;
        });

        return _data.join("");
    };

    utils.hex2ba = function(data) {
        var buffer = new ArrayBuffer(data.length/2);
        var view = new Uint8Array(buffer);

        for (var i = 0, y = 0; i < data.length; i += 2, y++) {
            view[y] = parseInt(data.substring(i, i + 2), 16);
        }
        return view;
    };

    utils.uuid = function() {
        var uuid = "", i, random;
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i == 8 || i == 12 || i == 16 || i == 20) {
                uuid += "-";
            }
            uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    };

    utils.searchMatch = function(haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    if (this.safestore === undefined) {
        this.safestore = {};
    }

    this.safestore.utils = utils;
    this.safestore.crypt = crypt;

    _.bindAll(this.safestore.utils);
    _.bindAll(this.safestore.crypt);
}).call(this);

var t_get_new_random_array = function() {
    var view = new Uint8Array([1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9]);
    return view;
};

var t_get_large_buffer = function() {
    var buffer = new ArrayBuffer(1024*64)
    var view = new Int32Array(buffer);
    window.crypto.getRandomValues(view);
    return view;
};

var t_cipher = function() {
    var buffer = new Int32Array([1,2,3,4,5,6,7,8,9,0]);

    var ec = safestore.crypt.encryptBuffer("kk", buffer);
    var dc = safestore.crypt.decryptBuffer("kk", ec);

    var view = new Int32Array(dc);
    return view;
};

var t_cipher_2 = function() {
    var iv = [4,3,2,1];
    var dataView = [1,2,3,4,5,6,7,8,9,10];

    var aes = new sjcl.cipher.aes([244234,322131,13123,123123]);

    var cryptedBuffer = sjcl.mode.cbc.encrypt(aes, dataView, iv);
    var decryptedView = sjcl.mode.cbc.decrypt(aes, cryptedBuffer, iv);

    return decryptedView;
};

var t_get_bytes = function() {
    var view = t_get_new_random_array()
    var bytes = ab2bytes(view.buffer);
    var hash = CryptoJS.SHA1(bytes).toString();
    return [bytes, hash];
};

var t_upload = function(bb) {
    var bytes = bb[0];
    var hash = bb[1];

    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/octet-stream");
    xhr.open("PUT", "http://localhost:8000/tests/binary-upload", true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4  && xhr.status == 200) {
            console.log(xhr.response, hash);
        }
    }
    xhr.send(bytes);
};

var t_upload_download = function(bb) {
    var bytes = bb[0];
    var originalHash = bb[1];

    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/octet-stream");
    xhr.open("PUT", "http://localhost:8000/tests/binary-upload", true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4  && xhr.status == 200) {
            console.log("UPLOAD FINISHED");

            var xhr2 = new XMLHttpRequest();
            xhr2.overrideMimeType("application/octet-stream");
            xhr2.open("GET", "http://localhost:8000" + xhr.response, true);

            xhr2.onreadystatechange = function() {
                if (xhr2.readyState == 4  && xhr2.status == 200) {
                    var hash = CryptoJS.SHA1(xhr2.response).toString();
                    console.log("DOWNLOAD FINISHED:", hash, originalHash);
                }
            };

            xhr2.send(null);
        }
    };

    xhr.send(bytes);
};

var t_upload_download_real_file = function() {
    var files = document.getElementById('file').files;
    if (files.length !== 1) {
        console.log("Select a file");
        return;
    }

    var file = files[0];

    var reader = new FileReader();
    reader.onload = function(e) {
        var result = e.target.result;
        var originalHash = CryptoJS.SHA1(result).toString();

        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/octet-stream");
        xhr.open("PUT", "http://localhost:8000/tests/binary-upload", true);

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4  && xhr.status == 200) {
                console.log("UPLOAD FINISHED");

                var xhr2 = new XMLHttpRequest();
                xhr2.overrideMimeType("application/octet-stream");
                xhr2.open("GET", "http://localhost:8000" + xhr.response, true);

                xhr2.onreadystatechange = function() {
                    if (xhr2.readyState == 4  && xhr2.status == 200) {
                        var hash = CryptoJS.SHA1(xhr2.response).toString();
                        console.log("DOWNLOAD FINISHED:", hash, originalHash);
                    }
                };

                xhr2.send(null);
            }
        };

        xhr.send(result);
    };

    reader.readAsBinaryString(file);
};
