var PrivkeyController = function($scope, $http, $location, $store, $crypto, $notify) {
    $store.instance.get(["privkey", "passphrase"], function(data) {
        $scope.privkey = data.privkey || "";
        $scope.passphrase = data.passphrase || "";
    });

    $scope.save = function() {
        $store.data.privkey = $scope.privkey;
        $store.data.passphrase = $scope.passphrase;

        $store.instance.set({"privkey":$scope.privkey, "passphrase":$scope.passphrase}, function() {
            $store.initializeKeys()
            $notify("info", ["Key initialized successfull"]);
        });
    };

    // File reader helper
    $scope.keyFileChange = function($event) {
        var fileslist = $event.target.files;
        if (fileslist.length > 0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $scope.$apply(function() {
                    $scope.privkey = e.target.result;
                });
            };
        };
        reader.readAsText(fileslist[0]);
    };

    $scope.clearKey = function() {
        $store.service.set({"privkey": undefined, "passphrase": undefined}, function() {
            $store.initializeKeys();
            $notify("info", ["Key cleared successfull"]);
        });

        $scope.privkey = "";
        $scope.passphrase = "";
    };
};

PrivkeyController.$inject = ['$scope', '$http', '$location', '$store', '$crypto', '$notify'];
