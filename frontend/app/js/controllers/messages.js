var translateMessage = function(message, $store, $crypto) {
    if ($store.keys.privKey !== undefined) {
        var key = $store.keys.privKey.decrypt(message.meta);

        if (message.content) {
            message.content = $crypto.decrypt(key, message.content);
        }

        if (message.response) {
            message.response = $crypto.decrypt(key, message.response);
        }

        message.subject = $crypto.decrypt(key, message.subject);
    }
    return message;
};


var MessagesController = function($scope, $http, $store, $urls, $crypto, $notify) {
    $scope.hideList = false;
    $scope.hideForm = true;
    $scope.hidePreview = true;
    $scope.contacts = [];
    $scope.messages = [];

    // Load contacts

    var contactsQ = $http({"method": "GET", "url": $urls.get('contacts'),
                            headers: {"X-SAFESTORE-TOKEN": $store.data.token}});

    contactsQ.success(function(data, status) {
        _.each(data, function(item) {
            _.defer(function() {
                $scope.$apply(function() {
                    $scope.contacts.push(translateContact(item, $store, $crypto));
                });
            });
        });
    });


    // Load messages

    var messagesQ = $http({"method": "GET", "url": $urls.get('messages'),
                            headers: {"X-SAFESTORE-TOKEN": $store.data.token}});

    messagesQ.success(function(data, status) {
        _.each(data, function(item) {
            _.defer(function() {
                $scope.$apply(function() {
                    $scope.messages.push(translateMessage(item, $store, $crypto));
                });
            });
        });
    });

    // Scope methods
    $scope.openMessageForm = function() {
        $scope.currentMessage = {};
        $scope.hideList = true;
        $scope.hideForm = false;
    };

    $scope.submit = function() {
        var message = $scope.currentMessage;
        var randomKey = $crypto.randomKey();

        var contact = _.find($scope.contacts, function(item) { return item.id == message.contactId; });
        var key = $crypto.parsePublicKey(contact.pubkey);

        var postData = {
            "subject": $crypto.encrypt(randomKey, message.subject),
            "content": $crypto.encrypt(randomKey, message.content),
            "to": message.contactId,
            "meta": key.encrypt(randomKey),
        };

        var promise = $http({"method":"PUT", "url": $urls.get("messages"),
                            "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            $notify("info", ["Message sent successfull to " + contact.alias]);
            $scope.hideList = false;
            $scope.hideForm = true;
            $scope.currentMessage = undefined;
        });
    };

    $scope.responseForm = {};

    $scope.submitResponse = function() {
        var message = $scope.currentMessage;

        var randomKey = $crypto.randomKey();
        var selfKey = $store.keys.privKey.decrypt(message.meta);

        var contact = _.find($scope.contacts, function(item) { return item.id == message.from_id; });
        var key = $crypto.parsePublicKey(contact.pubkey);

        var postData = {
            "content_self": $crypto.encrypt(selfKey, $scope.responseForm.content),
            "reply_to": message.id,

            "subject": $crypto.encrypt(randomKey, message.subject),
            "content": $crypto.encrypt(randomKey, $scope.responseForm.content),
            "to_id": message.from_id,
            "meta": key.encrypt(randomKey),
        };

        var promise = $http({"method":"PUT", "url": $urls.get("messages"),
                            "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});
        promise.success(function(data, status) {
            $scope.currentMessage.response = $scope.responseForm.content;
            $scope.responseForm = {};
        });
    };

    $scope.close = function() {
        $scope.hideList = false;
        $scope.hideForm = true;
        $scope.hidePreview = true;
    };

    $scope.idToName = function(id) {
        var contact = _.find($scope.contacts,
            function(item) { return item.id === id; });
        if (contact !== undefined) {
            return contact.alias;
        } else {
            return id;
        }
    };

    $scope.rehashChecked = function() {
        $scope.anyChecked = _.any($scope.messages, function(item) { return item.checked; });
    };

    $scope.previewMessage = function(message) {
        $scope.currentMessage = message;
        $scope.hideList = true;
        $scope.hidePreview = false;
    };

    $scope.deleteMessage = function() {
        var message = this.message;
        var promise = $http({"method":"DELETE", "url": $urls.get("message", message.id),
                            "headers": {"X-SAFESTORE-TOKEN": $store.data.token}});

        promise.success(function(data, status) {
            $scope.messages = _.reject($scope.messages, function(item) {
                                    return item.id == message.id; })
            $notify("info", ["Message deleted successfull"], 3000);
        });
    };
};

MessagesController.$inject = ['$scope', '$http', '$store', '$urls', '$crypto', '$notify'];
