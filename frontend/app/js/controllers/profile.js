var ProfileController = function($scope, $http, $store, $urls) {
    var promiseProfile = $http({"method": "GET", "url": $urls.get('profile'),
                            headers: {"X-SAFESTORE-TOKEN": $store.data.token}});

    promiseProfile.success(function(data, status) {
        $scope.profile = data;
    });
};

ProfileController.$inject = ['$scope', '$http', '$store', '$urls'];
