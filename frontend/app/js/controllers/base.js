var NotifyController = function($scope) {
    $scope.$watch('gmessages', function(newValue, oldValue) {
        if (newValue.info === undefined && newValue.error === undefined) {
            $scope.hasNotify = false;
        } else {
            $scope.hasNotify = true;
        }
    });

    $scope.clear = function(type) {
        delete $scope.gmessages[type];
    }

    $scope.gmessages = {};
    var timeoutSem = null;

    $scope.$on("$notify", function(ctx, type, message, timeout) {
        var msgs = {};

        if (timeoutSem !== null) {
            clearTimeout(timeoutSem);
            timeoutSem = null;
        }

        if (_.isArray(message)) {
            msgs[type] = message;
        } else {
            msgs[type] = [message];
        }

        $scope.gmessages = msgs;

        if (timeout !== undefined) {
            timeoutSem = setTimeout(function() {
                $scope.gmessages = {};
                $scope.$digest();
            }, timeout);
        }
    });
};

NotifyController.$inject = ['$scope'];
