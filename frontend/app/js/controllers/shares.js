var PendingSharesController = function($scope, $http, $store, $urls, $crypto, $notify) {
    var sharedNotesUrl = $urls.get('blobs') + "?" + $.param({"pending": "t"});

    var promise = $http({"method": "GET", "url": sharedNotesUrl, cache: false,
                            headers: {"X-SAFESTORE-TOKEN": $store.data.token}});
    promise.success(function(data, status) {
        _.each(data, function(item) {
            _.defer(function() {
                $scope.$apply(function() {
                    $scope.sharedNotes.push(translateNote(item, $store, $crypto));
                });
            });
        });
    });

    $scope.sharedNotes = [];
    $scope.hidePreview = true;
    $scope.hideList = false;

    $scope.previewSharedNote = function(note) {
        $scope.hidePreview = false;
        $scope.hideList = true;
        $scope.currentSharedNote = note;
    };

    $scope.acceptSharedNote = function(note) {
        var postData = {"accepted": true};
        var promise = $http({"method": "PATCH", "url": $urls.get('blob', note.id),
                        headers: {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            $scope.sharedNotes = _.filter($scope.sharedNotes, function(item) {
                return item.id != note.id;
            });

            if ($store.cache.notes !== undefined) {
                delete $store.cache.notes;
            }
        });
    };

    $scope.removeSharedNote = function(note) {
        var promise = $http({"method":"DELETE",  "url":$urls.get('blob', note.id),
                        "headers": {"X-SAFESTORE-TOKEN": $store.data.token}});

        promise.success(function(data, status) {
            $scope.sharedNotes = _.filter($scope.sharedNotes, function(item) {
                return item.id != note.id;
            });
        });
    };
};

PendingSharesController.$inject = ['$scope', '$http', '$store', '$urls', '$crypto', '$notify'];
