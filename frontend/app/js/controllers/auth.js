'use strict';

/* Controllers */

var LoginController = function($scope, $http, $location, $urls, $store) {
    $scope.title = "Safestore - Login";
    $scope.submit = function() {
        var promise = $http.post($urls.get('auth'), this.form.fields)

        promise.success(function(data, status) {
            $store.instance.set(data);
            $store.initializeKeys()
            $location.url("/notes");
        });

        promise.error(function(data, status, headers, config) {
            if (data.errors) {
                $scope.form.errors = data.errors;
            }
        });
    };

    $scope.clearFormErrors = function() {
        this.form.errors = [];
    };

    $scope.form = {
        fields: {
            username: "",
            password: "",
        },
        errors: []
    };

    // Cache values
    $store.instance.get('login:cache-id', function(data) {
        if (data !== undefined) {
            $scope.form.fields.username = data;
        }
    });

    $scope.$watch('form.fields.username', function(newVal, oldVal) {
        $store.instance.set("login:cache-id", newVal);
    });
};

LoginController.$inject = ['$scope', '$http', '$location', '$urls', '$store'];


var VerifyController = function($scope, $http, $location, $notify, $urls) {
    $scope.title = "Safestore - Verify";

    $scope.form = {
        fields: {token: ""},
    };


    $scope.submit = function() {
        var q = $http.post($urls.get('verify'), this.form.fields)

        q.success(function(data, status) {
            var message = "User " + data.id + " verified!";

            $notify("info", [message]);
            $location.url("/login");
        });

        q.error(function(data, status, headers, config) {
            $notify("error", [data]);
        });
    };
};

VerifyController.$inject = ['$scope', '$http', '$location', '$notify', '$urls'];


var RegisterController = function($scope, $http, $location, $notify, $urls) {
    $scope.title = "Safestore - Register";

    $scope.form = {
        fields: {
            username: safestore.utils.uuid(),
            email: "",
            password: "",
            pubkey: ""
        },
        errors: []
    };

    $scope.submit = function() {
        var promise = $http.post($urls.get('user'), this.form.fields)

        promise.success(function(data, status) {
            var message = "User created successfull. The new id is: " + data.id;
            message += " Now receive an email to validate your account.";

            $notify("info", [message]);
            $location.url("/verify");
        });

        promise.error(function(data, status, headers, config) {
            $scope.form.errors = [data];
        });
    };

    // File reader helper
    $scope.keyFileChange = function($event) {
        var fileslist = $event.target.files;
        if (fileslist.length > 0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $scope.$apply(function() {
                    $scope.form.fields.pubkey = e.target.result;
                });
            };
        };
        reader.readAsText(fileslist[0]);
    };
};

RegisterController.$inject = ['$scope', '$http', '$location', '$notify', '$urls'];
