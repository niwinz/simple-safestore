var translateNote = function(note, $store, $crypto) {
    if (note.translated) {
        return note;
    };

    if ($store.keys.privKey !== undefined) {
        var key = $store.keys.privKey.decrypt(note.meta);
        note.title = $crypto.decrypt(key, note.title);
        note.content = $crypto.decrypt(key, note.content);
        note.slug = _.str.slugify(note.title);
        note.translated = true;
    }

    return note;
};

var NotesController = function($scope, $http, $store, $urls, $crypto, $notify, loader) {
    $scope.addNote = function(note) {
        $store.cache.notes.unshift(note);
        $scope.notes.unshift(note);
    };

    $scope.updateNotes = function(notes) {
        $store.cache.notes = notes;
        $scope.notes = notes;
    };

    $scope.init = function(nocache) {
        $scope.notesLoadedPercent = 0;
        $scope.notesLoaded = false;
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 10;
        $scope.pagedItems = [];
        $scope.currentPage = 0;

        if ($store.cache.notes && !nocache) {
            $scope.notes = $store.cache.notes;
            $scope.notesLoadedPercent = 100;
            $scope.notesLoaded = true;
            $scope.search();
        } else {
            loader.loadNotes().progress(function(data) {
                $scope.$apply(function() {
                    $scope.notesLoadedPercent = data;
                });
            }).then(function(data) {
                $scope.$apply(function() {
                    $scope.notesLoaded = true;
                    $scope.notesLoadedPercent = 100;

                    $scope.notes = data;
                    $store.cache.notes = data;
                    $scope.search();
                });
            });
        }
    };

    $scope.search = function() {
        var searchMatch = safestore.utils.searchMatch;

        $scope.filteredItems = _.filter($scope.notes, function(item) {
            if (searchMatch(item.title, $scope.query) || searchMatch(item.content, $scope.query)) {
                return true;
            } else {
                return false;
            }
        });

        $scope.groupToPages();
    };


    $scope.groupToPages = function() {
        $scope.pagedItems = [];

        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
    };

    $scope.init(false);

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function () {
        $scope.currentPage = this.n;
    };

    $scope.range = function(start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };


    // Static model definition
    $scope.formOpen = false;
    $scope.formPreview = false;
    $scope.selectedNote = undefined;
    $scope.selectedContact = undefined;

    $scope.openForm = function(note) {
        $scope.selectedNote = note || {};
        if (note === undefined) {
            $scope.selectedNote.isNew = true;
        } else {
            if (note.url === undefined) {
                var blob = new Blob([note.content], {type:"text/plain"});
                $scope.selectedNote.url = URL.createObjectURL(blob);
            }
        }

        $scope.formOpen = true;
    };

    $scope.closeForm = function() {
        if ($scope.selectedNote.url !== undefined) {
            URL.revokeObjectURL($scope.selectedNote.url);
            delete $scope.selectedNote.url;
        }

        $scope.selectedNote = undefined;
        $scope.formOpen = false;
    };

    $scope.submit = function() {
        var note = $scope.selectedNote;
        var randomKey = $crypto.randomKey();
        var postData = {
            "title": $crypto.encrypt(randomKey, note.title),
            "content": $crypto.encrypt(randomKey, note.content),
            "meta": $store.keys.pubKey.encrypt(randomKey)
        };

        if (note.id === undefined) {
            var promise = $http({"method":"POST", "url": $urls.get('blobs'),
                    "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

            promise.success(function(data, status) {
                $scope.addNote(translateNote(data, $store, $crypto));
                $scope.search();
            });
        } else {
            $http({"method":"PATCH", "url": $urls.get('blob', note.id),
                    "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});
        }

        $scope.closeForm();
    };

    $scope.removeSelected = function() {
        var checkedNotes = _.filter($scope.notes, function(item) {
                                    return item.checked || false; });

        var postData = {"blobs": _.map(checkedNotes, function(item) { return item.id; })};
        var promise = $http({"method":"DELETE",  "url":$urls.get('blobs'),
                        "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            var notes = _.reject($scope.notes, function(item) {
                                    return item.checked || false; })

            $scope.updateNotes(notes);
            $scope.rehashChecked();
            $scope.search();
        });
    };

    $scope.removeNote = function(note) {
        var promise = $http({"method":"DELETE",  "url": $urls.get('blob', note.id),
                        "headers": {"X-SAFESTORE-TOKEN": $store.data.token}});

        promise.success(function(data, status) {
            var notes = _.filter($scope.notes, function(item) {
                return item.id != note.id;
            });

            $scope.updateNotes(notes);
            $scope.search();
        });
    };

    $scope.openMerge = function() {
        var checked = _.filter($scope.notes, function(item) {return item.checked || false; });
        var patch = new diff_match_patch();

        $scope.mergeFirst = checked[0];
        $scope.mergeSecond = checked[1];

        var diff = patch.diff_main(checked[0].content, checked[1].content);
        patch.diff_cleanupSemantic(diff);

        var patches = patch.patch_make(diff);

        $scope.prettyPatch = patch.diff_prettyHtml(diff);
        $scope.textPatch = patch.patch_toText(patches);
        $scope.mergeOpen = true;
    };

    $scope.shareNote = function() {
        var note = this.note;
        var key = $crypto.parsePublicKey($scope.selectedContact.pubkey);
        var randomKey = $crypto.randomKey();

        var postData = {
            "title": $crypto.encrypt(randomKey, note.title),
            "content": $crypto.encrypt(randomKey, note.content),
            "meta": key.encrypt(randomKey),
            "receiver_id": $scope.selectedContact.id
        };

        var promise = $http({"method":"POST", "url": $urls.get('blob-share', note.id),
                        "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            $notify("info", ["Note shared successfull with " + $scope.selectedContact.alias], 5000);
        });
    };

    $scope.rehashChecked = function() {
        var checked = _.filter($scope.notes, function(item) {
                                    return item.checked || false; })
        $scope.anyChecked = (checked.length > 0);
        $scope.twoChecked = (checked.length === 2);
    };

    /* Clean selectedNote model on form is closed */
    $scope.$watch('formOpen', function(newValue, oldValue) {
        if (newValue === false && oldValue === true) {
            $scope.selectedNote = undefined;
        }
    });

    $scope.$on('change:contact', function(event, contact) {
        $scope.selectedContact = contact;
    });
};

NotesController.$inject = ['$scope', '$http', '$store', '$urls', '$crypto', '$notify', 'loader'];

var translateContact = function(contact, $store, $crypto) {
    if ($store.keys.privKey !== undefined) {
        var key = $store.keys.privKey.decrypt(contact.meta);
        contact.alias = $crypto.decrypt(key, contact.alias);
    }
    return contact;
};

var ContactsController = function($scope, $http, $store, $urls, $crypto, loader) {
    $scope.contacts = [];
    $scope.formEditOpen = false;
    $scope.formNewOpen = false;
    $scope.formErrors = [];
    $scope.selectedContact = undefined;

    loader.loadContacts().progress(function(item) {
        $scope.$apply(function() {
            $scope.contactsLoaded = true;
            $scope.contacts.push(item);
        });
    }).then(function() {
        $scope.$apply(function() {
            $scope.contactsLoaded = true;
        });
    });

    $scope.submitNew = function() {
        var contact = $scope.newContact;
        var randomKey = $crypto.randomKey();

        var postData = {
            "alias": $crypto.encrypt(randomKey, contact.alias),
            "meta": $store.keys.pubKey.encrypt(randomKey),
            "user_id": contact.id
        };

        var promise = $http({"method":"POST", "url": $urls.get('contacts'),
                "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            $scope.contacts.push(translateContact(data, $store, $crypto));
            $scope.formNewOpen = false;
            $scope.newContact = undefined;
        });

        promise.error(function(data, status) {
            $scope.formErrors = [data];
        });
    };

    $scope.submitEdit = function() {
        var contact = $scope.selectedContact;
        var randomKey = $crypto.randomKey();

        var postData = {
            "alias": $crypto.encrypt(randomKey, contact.alias),
            "meta": $store.keys.pubKey.encrypt(randomKey)
        };

        var promise = $http({"method":"PATCH", "url": $urls.get('contact', contact.id),
                "headers": {"X-SAFESTORE-TOKEN": $store.data.token}, 'data':postData});

        promise.success(function(data, status) {
            $scope.formEditOpen = false;
        });

        promise.error(function(data, status) {
            $scope.formErrors = [data];
        });
    };

    $scope.openFormEdit = function() {
        if ($scope.selectedContact !== undefined) {
            $scope.formEditOpen = true;
        }
    };

    $scope.openFormNew = function() {
        $scope.newContact = {};
        $scope.formNewOpen = true;
    };

    $scope.closeFormEdit = function() {
        $scope.formEditOpen = false;
    };

    $scope.closeFormNew = function() {
        $scope.formNewOpen = false;
    };

    $scope.changeSelection = function(contact) {
        if (contact === $scope.selectedContact) {
            delete contact.selected;
            delete $scope.selectedContact;
            $scope.$emit("change:contact", null);
        } else {
            $scope.selectedContact = contact;

            // TODO: optimize this.
            _.each($scope.contacts, function(item) {
                if (item.selected) {
                    delete item.selected;
                }
            });

            contact.selected = true;
            $scope.$emit("change:contact", contact);
        }
    };

    $scope.deleteSelectedContact = function() {
        var contact = $scope.selectedContact;
        if (contact !== undefined) {

            var promise = $http({method:"DELETE", url: $urls.get('contact', contact.id),
                            "headers": {"X-SAFESTORE-TOKEN": $store.data.token}})

            promise.success(function(data, status) {
                $scope.contacts = _.filter($scope.contacts, function(item) {
                    return item != contact;
                });

                $scope.selectedContact.selected = false;
                $scope.selectedContact = undefined
            });
        }
    }
};

ContactsController.$inject = ['$scope', '$http', '$store', '$urls', '$crypto', 'loader'];
