//mocha.setup({ignoreLeaks: true});

describe("safestore crypt tests", function() {
    it("crypt.randomIv returns valid array type", function() {
        var iv = safestore.crypt.randomIv();
        chai.expect(iv).is.instanceOf(Int32Array);
    });

    it("crypt.randomIv returns valid array length", function() {
        var iv = safestore.crypt.randomIv();
        chai.expect(iv.length).is.equal(4);
    });

    it("text encrypt decrypt should works correctly", function() {
        var encryptedData = safestore.crypt.encryptText("password", "kk");
        var decryptedData = safestore.crypt.decryptText("password", encryptedData);
        chai.expect(decryptedData).is.equal("kk");
    });
});


