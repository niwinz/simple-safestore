PIDCRYPT_SOURCES=app/lib/crypt/pidcrypt.js app/lib/crypt/pidcrypt_util.js app/lib/crypt/asn1.js app/lib/crypt/jsbn.js app/lib/crypt/rng.js app/lib/crypt/prng4.js app/lib/crypt/rsa.js app/lib/crypt/md5.js app/lib/crypt/aes_core.js app/lib/crypt/aes_cbc.js

EXTERN_SOURCES=app/lib/q.js app/lib/diff_match_patch_uncompressed.js app/lib/jquery-1.9.1.js app/lib/underscore.js app/lib/underscore.string.js app/lib/bootstrap.js app/lib/parsley.js app/lib/moment.js app/lib/angular/angular.js app/lib/base64.js app/lib/spin.js

CRYPTOJS_SOURCES=app/lib/cryptojs/core.js app/lib/cryptojs/x64-core.js app/lib/cryptojs/enc-base64.js app/lib/cryptojs/sha1.js app/lib/cryptojs/sha3.js app/lib/cryptojs/md5.js app/lib/cryptojs/pbkdf2.js app/lib/cryptojs/evpkdf.js app/lib/cryptojs/cipher-core.js app/lib/cryptojs/format-hex.js app/lib/cryptojs/aes.js app/lib/cryptojs/mode-cfb.js app/lib/cryptojs/mode-ecb.js

SJCL_SOURCES=app/lib/sjcl/sjcl.js app/lib/sjcl/aes.js app/lib/sjcl/bitArray.js app/lib/sjcl/codecString.js app/lib/sjcl/codecBytes.js app/lib/sjcl/codecHex.js app/lib/sjcl/codecBase64.js app/lib/sjcl/sha256.js app/lib/sjcl/ccm.js app/lib/sjcl/ocb2.js     app/lib/sjcl/gcm.js app/lib/sjcl/hmac.js app/lib/sjcl/pbkdf2.js app/lib/sjcl/random.js app/lib/sjcl/convenience.js app/lib/sjcl/cbc.js

APP_SOURCES=app/js/app.js app/js/utils.js app/js/storage/html5-localstorage.js app/js/services/common.js app/js/services/storage.js app/js/controllers/base.js app/js/controllers/auth.js app/js/controllers/messages.js app/js/controllers/notes.js app/js/controllers/profile.js app/js/controllers/privkey.js app/js/controllers/shares.js app/js/filters.js app/js/directives/common.js
