"user strict";

(function() {
    var NiwiWorker = function(scriptName) {
        var defer = Q.defer();
        this.worker = new Worker(scriptName);

        this.worker.onmessage = function(e) {
            var data = e.data;
            if (data.event === "progress") {
                defer.notify(data.data);
            } else {
                defer.resolve(data.data);
            }
        };

        this.worker.onerror = function(e) {
            var errorMsg = "Error line:" + e.lineno + " in " + e.filename + ": " + e.message;
            defer.reject(new Error(errorMsg));
        };

        var self = this;

        defer.promise.fin(function() {
            self.worker.terminate();
            console.log("WORKER TERMINATED");
        });

        this.promise = defer.promise;
    };

    NiwiWorker.prototype.sendMessage = function(message) {
        this.worker.postMessage(message);
    };

    this.NiwiWorker = NiwiWorker;
}).call(this);

