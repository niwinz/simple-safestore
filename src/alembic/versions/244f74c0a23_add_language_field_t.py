"""Add language field to user

Revision ID: 244f74c0a23
Revises: None
Create Date: 2013-02-10 11:14:19.105674

"""

# revision identifiers, used by Alembic.
revision = '244f74c0a23'
down_revision = None

from alembic import op
import sqlalchemy as sa

from sqlalchemy.schema import Column
from sqlalchemy.types import Unicode


def upgrade():
    op.add_column('user', Column("lang", Unicode(10), default="en"))


def downgrade():
    op.drop_column("user", "lang")
