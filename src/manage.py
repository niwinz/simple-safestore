#!/usr/bin/env python
import os
import sys

sys.path.insert(0, '/home/niwi/devel/django-superview')
sys.path.insert(0, '/home/niwi/devel/py-mhash')
sys.path.insert(0, '/home/niwi/devel/djorm-ext-pool')
sys.path.insert(0, '/home/niwi/devel/djorm-ext-core')
sys.path.insert(0, '/home/niwi/devel/djorm-ext-expressions')
sys.path.insert(0, '/home/niwi/devel/djorm-ext-hstore')
sys.path.insert(0, '/home/niwi/devel/djorm-ext-pgbytea')

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "safestore.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
