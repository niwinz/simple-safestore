# -*- coding: utf-8 -*-

"""
Posible database dict configuration:

SQLALCHEMY_DATABASES = {
    'default': {
        'connect_string': 'postgresql://user@host/dbname',
        'options': {
            'server_side_cursors': True,
        }
    },
}

"""

from django.core.exceptions import ImproperlyConfigured
from django.conf import settings

from sqlalchemy import create_engine
from sqlalchemy import event
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session


def on_pool_connection_checkout(dbapi_connection, connection_record, connection_proxy):
    cursor = connection_proxy.cursor()
    cursor.execute('SET TIME ZONE %s;', [settings.TIME_ZONE])


class EngineManager(object):
    def __init__(self, databases):
        self.databases = databases
        self._engines = {}

    def create_engine(self, alias):
        """
        Creates on demand local engine for alias.
        """

        if alias not in self.databases:
            raise ImproperlyConfigured("Alias '%s' is not found in database configuration." % (alias))

        connection_string = self.databases[alias]['connect_string']
        options = self.databases[alias].get('options', {})
        return create_engine(connection_string, **options)

    def __getitem__(self, alias):
        if alias in self._engines:
            return self._engines[alias]

        self._engines[alias] = self.create_engine(alias)
        #event.listen(self._engines[alias], 'checkout', on_pool_connection_checkout)
        return self._engines[alias]


class SessionManager(object):
    def __init__(self):
        self._scoped_sessions = {}
        self._scoped_access = {}

        self._sessions = {}

    def delete_scoped(self, alias):
        if alias in self._scoped_sessions:
            self._scoped_sessions[alias].remove()

    def remove_scoped_all(self, alias='default'):
        if alias in self._scoped_sessions.keys():
            self._scoped_sessions[alias].remove()

    def remove_scoped_accessed(self):
        for alias in self._scoped_access.keys():
            self._scoped_sessions[alias].remove()

        self._scoped_access = {}

    def create_scoped_session(self, alias):
        # TODO: parametrize on settings kwargs to sessionmaker
        return scoped_session(sessionmaker(bind=engines[alias],
                                        expire_on_commit=False))

    def get_scoped_session(self, alias='default', *args, **kwargs):
        if alias in self._scoped_sessions:
            self._scoped_access[alias] = 1
            return self._scoped_sessions[alias](*args, **kwargs)

        self._scoped_sessions[alias] = self.create_scoped_session(alias)
        self._scoped_access[alias] = 1
        return self._scoped_sessions[alias](*args, **kwargs)

    def get_session(self, alias='default', bind=None, *args, **kwargs):
        if alias in self._sessions:
            return self._sessions[alias](*args, **kwargs)

        if bind is None:
            bind = engines[alias]

        self._sessions[alias] = sessionmaker(bind=bind, expire_on_commit=False)
        return self._sessions[alias](*args, **kwargs)


engines = EngineManager(settings.SQLALCHEMY_DATABASES)
engine = engines['default']

sessions = SessionManager()
