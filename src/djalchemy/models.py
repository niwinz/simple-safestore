# -*- coding: utf-8 -*-

from django.core import signals
from djalchemy.core import sessions

def on_request_is_finished(*args, **kwargs):
    sessions.remove_scoped_accessed()

signals.request_finished.connect(on_request_is_finished, dispatch_uid="on-request-finished")
signals.got_request_exception.connect(on_request_is_finished, dispatch_uid="on-request-exception")
