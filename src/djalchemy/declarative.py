# -*- coding: utf-8 -*-

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer

class BasePrototype(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

BaseModel = declarative_base(cls=BasePrototype)

__all__ = ['BaseModel']
