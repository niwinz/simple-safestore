# -*- coding: utf-8 -*-

import functools
import types

from .core import sessions

# Context managers

class transaction(object):
    """
    Transaction block that call commit on session
    on is finihed.
    """

    def __init__(self, session=None):
        if session is None:
            self.session = sessions.get_scoped_session()
        else:
            self.session = session

    def __enter__(self):
        self.session.begin(subtransactions=True)

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            if exc_value is not None:
                self.session.rollback()
            else:
                self.session.commit()
        except:
            self.session.rollback()
            raise


# Decorators

def transaction_method(*args, **kwargs):
    nested = kwargs.get('nested', True)
    ensure_session = kwargs.get('ensure_session', True)

    def _decorator(method):
        @functools.wraps(method)
        def _wrapper(self, *_args, **_kwargs):
            session = sessions.get_scoped_session()

            # Session initial state is always on BEGIN. So, because
            # it, we can not begin again without a subtransaction.
            if nested:
                session.begin_nested()

            try:
                if ensure_session:
                    response = method(self, session, *_args, **_kwargs)
                else:
                    response = method(self, *_args, **_kwargs)

                session.commit()
                return response
            except Exception as e:
                session.rollback()
                raise

        return _wrapper

    method_types = (types.FunctionType, types.LambdaType, types.MethodType)
    if len(args) == 1 and isinstance(args[0], method_types):
        return _decorator(args[0])
    return _decorator


def transactional_view(cls):
    """
    Ensure django view exectution context into a sqlalchemy
    transaction context.
    """

    class View(cls):
        @transaction_method(nested=False)
        def dispatch(self, session, *args, **kwargs):
            self.session = session
            return super(View, self).dispatch(*args, **kwargs)

    return View
