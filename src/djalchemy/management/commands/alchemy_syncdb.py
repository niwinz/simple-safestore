# -*- coding: utf-8 -*-

from optparse import make_option
import sys
import traceback
import logging

from django.conf import settings
from django.core.management.base import NoArgsCommand
from django.core.management.color import no_style
from django.utils.datastructures import SortedDict
from django.utils.importlib import import_module

from djalchemy.core import engines
from djalchemy.declarative import BaseModel


class Command(NoArgsCommand):
    option_list = NoArgsCommand.option_list + (
        make_option('--noinput', action='store_false', dest='interactive', default=True,
            help='Tells Django to NOT prompt the user for input of any kind.'),
        make_option('--database', action='store', dest='database',
            default='default', help='Nominates a database to synchronize. '
                'Defaults to the "default" database.'),
    )
    help = "Create the database tables for all apps in INSTALLED_APPS whose tables haven't already been created."

    def handle_noargs(self, **options):
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

        verbosity = int(options.get('verbosity'))
        interactive = options.get('interactive')
        show_traceback = options.get('traceback')
        self.style = no_style()

        db = options.get('database')
        engine = engines[db]

        for app_path in settings.INSTALLED_APPS:
            try:
                app_mod = import_module(app_path + '.models')
            except ImportError:
                continue

            if verbosity >= 1:
                print("SQLALchemy: Installing tables from {0}...".format(app_path))

        BaseModel.metadata.create_all(engine)
