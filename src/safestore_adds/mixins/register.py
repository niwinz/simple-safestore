# -*- coding: utf-8 -*-

import re

from django.conf import settings
from safestore.utils.views.exceptions import ValidateException
from safestore.utils.views import generic, decorators


ADDS_LIMITER = getattr(settings, "ADDS_LIMITER", True)
ADDS_LIMIT_REGEXLIST = getattr(settings, "ADDS_LIMIT_REGISTER_REGEXLIST", [])


class LimitedRegister(object):
    def _validate_register(self, body):
        super(LimitedRegister, self)._validate_register(body)

        if ADDS_LIMITER:
            _has_valid_regex = False

            for rx in ADDS_LIMIT_REGEXLIST:
                _has_valid_regex = re.match(rx, body['email'])
                if _has_valid_regex:
                    break

            if not _has_valid_regex:
                raise ValidateException("Your email is not on the guest list!")


def patch_register():
    from safestore.api.views import user as user_views

    class User(LimitedRegister, user_views.User):
        pass

    if not hasattr(user_views, "_user_patched"):
        user_views._user_patched = True
        user_views._User = user_views.User
        user_views.User = User
