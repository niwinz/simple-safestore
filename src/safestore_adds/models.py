# -*- coding: utf-8 -*-

from django.conf import settings

from sqlalchemy.schema import Column, Sequence, ForeignKey, UniqueConstraint, PrimaryKeyConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import BigInteger, DateTime, Integer, Unicode, UnicodeText, Boolean
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import text

from djalchemy.declarative import BaseModel
from djalchemy.core import sessions

import uuid


