This directory must be containsa node keypair, is only used if
a distributed network of nodes is implemented.

Generate a keypair:

    openssl genrsa -out privkey.pem 2048
    openssl rsa -in privkey.pem -pubout -out pubkey.pem
