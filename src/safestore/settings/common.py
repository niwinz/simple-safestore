# Django settings for safestore project.

import os, sys
from django.utils.translation import ugettext_lazy as _

DEBUG = False
TEMPLATE_DEBUG = DEBUG

PROJECT_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
)

OUT_PROJECT_ROOT = os.path.abspath(
    os.path.join(PROJECT_ROOT, "..")
)

from .defaults import *

LOGS_PATH = os.path.join(PROJECT_ROOT, 'logs')

ADMINS = (
    ('Andrey Antukh', 'niwi@niwi.be'),
)

MANAGERS = ADMINS


SQLALCHEMY_DATABASES = {
    'default': {
        'connect_string': 'postgresql+psycopg2://localhost/safestore',
        'options': {
            'server_side_cursors': False,
            'client_encoding': 'utf-8',
            'isolation_level': 'SERIALIZABLE',
        }
    },
}

CACHES = {
    "default": {
        "BACKEND": "redis_cache.cache.RedisCache",
        "LOCATION": "127.0.0.1:6379:1",
        "OPTIONS": {
            "CLIENT_CLASS": "redis_cache.client.DefaultClient",
        }
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'test',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}



if 'test' in sys.argv:
    if "settings" not in ",".join(sys.argv):
        print("\033[1;91mNot settings specified.\033[0m")
        print("Try: \033[1;33mpython manage.py test --settings="
               "safestore.settings.testing -v2 \033[0m")
        sys.exit(0)


TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'

APPEND_SLASH = False
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEFAULT_CONTENT_TYPE = 'text/plain'

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_ROOT, "static")
STATIC_URL = '/static/'

STATICFILES_DIRS = []

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

SECRET_KEY = 'g8*kc%2+v)zx4ci8j4+kq9@u%+98=%(0owh59+bhlkq5!+(@pk'

TEMPLATE_LOADERS = (
    'django_jinja.loaders.AppLoader',
    'django_jinja.loaders.FileSystemLoader',
)

DEFAULT_JINJA2_TEMPLATE_EXTENSION = '.jinja'

MIDDLEWARE_CLASSES = [
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'safestore.middleware.crossdomain.CrossDomainMiddleware',
    'safestore.middleware.auth.AuthMiddleware',
]

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

TEMPLATE_CONTEXT_PROCESSORS = []
ROOT_URLCONF = 'safestore.urls'
WSGI_APPLICATION = 'safestore.wsgi.application'
TEMPLATE_DIRS = ()

INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'djalchemy',
    'safestore.storage',
    'safestore.api',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'simple': {
            'format': '%(levelname)s:%(asctime)s: %(message)s',
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django.request': {
            #'handlers': ['mail_admins'],
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
