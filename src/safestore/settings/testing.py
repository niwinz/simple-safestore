# -*- coding: utf-8 -*-
from .common import *
from .local import *

SQLALCHEMY_DATABASES['default']['connect_string'] = 'postgresql+psycopg2://localhost/test'
#SQLALCHEMY_DATABASES['default']['connect_string'] = 'sqlite:////tmp/foo.sqlite'
#del SQLALCHEMY_DATABASES['default']['options']

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake'
    }
}

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

SAFESTORE_PUBKEY = os.path.join(OUT_PROJECT_ROOT, 'keys', 'sample-pubkey.pem')
SAFESTORE_PRIVKEY = os.path.join(OUT_PROJECT_ROOT, 'keys', 'sample-privkey.pem')

SAFESTORE_TRUSTED_NODES = {
    'nodes-eu01.safestoreproject.co.nz':  os.path.join(OUT_PROJECT_ROOT, 'keys', 'sample-pubkey.pem')
}
