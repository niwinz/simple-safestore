# -*- coding: utf-8 -*-

# Utils
import os, sys
_project_root = os.path.abspath(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

_out_project_root = os.path.abspath(
    os.path.join(_project_root, ".."))


# Default safestore settings
SAFESTORE_DOMAIN='eu-01.nodes.safestoreproject.eu'
SAFESTORE_PUBKEY = os.path.join(_out_project_root, 'keys', 'pubkey.pem')
SAFESTORE_PRIVKEY = os.path.join(_out_project_root, 'keys', 'privkey.pem')

SAFESTORE_TRUSTED_NODES = {
    'eu-02.nodes.safestoreproject.eu':  os.path.join(_out_project_root, 'keys', 'node-eu02.pem')
}

SAFESTORE_TOKEN_TIMEOUT = 84000
SAFESTORE_MAX_BLOBS = 100
