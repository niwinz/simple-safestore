# -*- coding: utf-8 -*-

from django.contrib.auth.hashers import make_password, check_password
from django.core.files.storage import default_storage
from django.conf import settings

from sqlalchemy.schema import Column, Sequence, ForeignKey, UniqueConstraint, PrimaryKeyConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import BigInteger, DateTime, Integer, Unicode, UnicodeText, Boolean
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import text

from djalchemy.declarative import BaseModel
from djalchemy.core import sessions
from safestore.utils import crypto

import uuid


class User(BaseModel):
    id = Column(Unicode(40),  primary_key=True,
                    default=lambda: str(uuid.uuid1()))

    # Main fields
    pubkey = Column(UnicodeText(), default="")
    token = Column(Unicode(250), default="")
    is_active = Column(Boolean, default=False)
    email_hash = Column(Unicode(255), unique=True)

    username = Column(Unicode(500), unique=True)
    password = Column(Unicode(250))

    # Profile fields
    only_contacts = Column(Boolean(), default=True)
    max_blobs = Column(Integer, default=settings.SAFESTORE_MAX_BLOBS)
    blacklist = Column(UnicodeText())
    lang = Column(Unicode(10), default="en")

    # Relations
    blobs = relationship("Blob", backref=backref("user"), foreign_keys="User.id",
                    uselist=True, primaryjoin="Blob.owner_id == User.id",
                    lazy="dynamic")

    contacts = relationship("Contact", backref=backref("user"), foreign_keys="User.id",
                    uselist=True, primaryjoin="Contact.owner_id == User.id",
                    lazy="dynamic")

    def __str__(self):
        return self.id

    def set_password(self, password):
        self.password = make_password(password)

    def check_password(self, password):
        return check_password(password, self.password)


class Blob(BaseModel):
    id = Column(Unicode(40),  primary_key=True,
                    default=lambda: str(uuid.uuid1()))

    owner_id = Column(Unicode(40), index=True)
    created_at = Column(DateTime(timezone=True),
                    server_default=text("now()"))

    parent_owner_id = Column(Unicode(40))
    parent_blob_id = Column(Unicode(40))

    title = Column(Unicode(1024))
    content = Column(UnicodeText())

    accepted = Column(Boolean(), default=True)
    meta = Column(Unicode(2000), nullable=True)


class Contact(BaseModel):
    id = Column(Unicode(40),  primary_key=True,
                    default=lambda: str(uuid.uuid1()))

    owner_id = Column(Unicode(40), index=True)
    user_id = Column(Unicode(40), index=True)

    alias = Column(Unicode(255), default="")
    domain = Column(Unicode(255), default="")
    pubkey = Column(UnicodeText())

    # Generic field for store any data
    meta = Column(Unicode(2000), nullable=True)

    __table_args__ = (
        UniqueConstraint('owner_id', 'user_id'),
    )

    @property
    def is_local(self):
        return (not self.domain or
            self.domain == settings.SAFESTORE_DOMAIN)


class Message(BaseModel):
    id = Column(Unicode(40),  primary_key=True,
                    default=lambda: str(uuid.uuid1()))

    owner_id = Column(Unicode(40))
    recipient_id = Column(Unicode(40))

    reply_to = Column(Unicode(40), index=True)
    subject = Column(Unicode(500)) # stores encrypted text for recipient
    content = Column(UnicodeText()) # stores encrypted text for recipient
    response = Column(UnicodeText()) # stores a response from a recipient
    created_at = Column(DateTime(timezone=True),
                    server_default=text("now()"))

    # Generic field for store any data
    meta = Column(Unicode(2000), nullable=True)

