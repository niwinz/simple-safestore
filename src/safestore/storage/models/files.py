# -*- coding: utf-8 -*-

import uuid

from django.contrib.auth.hashers import make_password, check_password
from django.core.files.storage import default_storage
from django.conf import settings

from sqlalchemy.schema import Column, Sequence, ForeignKey, UniqueConstraint, PrimaryKeyConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import BigInteger, DateTime, Integer, Unicode, UnicodeText, Boolean
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import text

from djalchemy.declarative import BaseModel
from djalchemy.core import sessions
from safestore.utils import crypto


class FilePointer(BaseModel):
    id = Column(Unicode(40),  primary_key=True,
                    default=lambda: str(uuid.uuid1()))

    owner_id = Column(Unicode(40))
    file_id = Column(Unicode(40))

    meta = Column(Unicode(2000), nullable=True)
    name = Column(Unicode(500))

    __table_args__ = (
        UniqueConstraint('owner_id', 'name'),
    )


class File(BaseModel):
    id = Column(Unicode(40), primary_key=True,
        index=True, default=lambda: str(uuid.uuid1()))

    created_at = Column(DateTime(timezone=True),
                    server_default=text("now()"))

    delete = Column(Boolean, default=False)
    finished = Column(Boolean, default=False)
    total_parts = Column(Integer, default=0)


class FilePart(BaseModel):
    id = Column(Unicode(40), primary_key=True,
        index=True, default=lambda: str(uuid.uuid1()))

    file_id = Column(Unicode(40));
    order = Column(Integer)

    path = Column(Unicode(1000))
    size = Column(Integer, nullable=True, default=None)

    created_at = Column(DateTime(timezone=True),
                    server_default=text("now()"))

    __table_args__ = (
        UniqueConstraint('file_id', 'order'),
    )
