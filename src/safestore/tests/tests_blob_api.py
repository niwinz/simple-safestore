# -*- coding: utf-8 -*-

import json
from django.core.urlresolvers import reverse

from safestore.storage import models
from safestore.utils.test import patch_request_factory;
from safestore.utils.json import to_json

from djalchemy.core import sessions

from .utils import SafestoreTestCase

patch_request_factory()


class BlobTests(SafestoreTestCase):
    def tearDown(self):
        super(BlobTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.query(models.Blob).delete()
        self.session.commit()

    def test_create_blob(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()

        url = reverse('api:blobs')
        data = to_json({"title": "Sample","content": "foooo", "meta":""})

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"id", response.content)

    def test_access_to_existent_blob_01(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:blob', args=[blob.id])
        response = self.client.get(url, **authkwargs)

        self.assertEqual(response.status_code, 200)
        obj = json.loads(response.content.decode('utf-8'))

        self.assertIn("id", obj)
        self.assertIn("content", obj)

    def test_access_to_existent_blob_02(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()
        url = reverse('api:blob', args=["udfsadf-asdasd-asdasd-asdad"])
        response = self.client.get(url, **authkwargs)

        self.assertEqual(response.status_code, 404)

    def test_modify_existent_blob_01(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:blob', args=[blob.id])
        data = to_json({"content": "fooo", "meta":"dddd", "title":"aaa"})

        response = self.client.patch(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 200)

        self.session.rollback()
        blob2 = self.session.query(models.Blob).filter_by(id=blob.id).one()
        self.assertEqual(blob2.content, "fooo")

    def test_modify_existent_blob_02(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:blob', args=["udfsadf-asdasd-asdasd-asdad"])
        data = to_json({"content": "fooo"})

        response = self.client.patch(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 404)

    def test_get_blob_list(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:blobs')

        response = self.client.get(url, content_type="application/json",
                                                            **authkwargs)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)

    def test_delete_all_blobs_01(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        data = to_json({"blobs": [blob.id]})
        url = reverse('api:blobs')
        response = self.client.delete(url, data=data,
                        content_type="application/json", **authkwargs)
        self.assertEqual(response.status_code, 204)

    def test_delete_all_blobs_02(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()
        data = to_json({})

        url = reverse('api:blobs')
        response = self.client.delete(url, data=data,
                        content_type="application/json", **authkwargs)
        self.assertEqual(response.status_code, 400)

    def test_delete_all_blobs_03(self):
        user = self._create_user()
        blob = self._create_blob(user)

        self.session.commit()
        authkwargs, token = self._login()

        data = to_json({"blobs": [blob.id, "ddd-ddd-ddd"]})

        url = reverse('api:blobs')
        response = self.client.delete(url, data=data,
                        content_type="application/json", **authkwargs)
        self.assertEqual(response.status_code, 404)
