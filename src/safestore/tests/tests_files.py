# -*- coding: utf-8 -*-

import json
import unittest
from unittest.mock import Mock, MagicMock

from django.core import mail
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.test.client import Client

from safestore.storage import models
from safestore.utils import crypto
from safestore.utils.json import to_json
from safestore.utils.exceptions import ValidateException
from safestore.api.views.files import FilesService, Files
from safestore.utils.test import patch_request_factory

from djalchemy.core import sessions

from .utils import SafestoreTestCase


patch_request_factory()


class FilesTestCase(SafestoreTestCase):
    def setUp(self):
        super(FilesTestCase, self).setUp()
        self.user = self._create_user()
        self.service = FilesService(self.user)

    def tearDown(self):
        super(FilesTestCase, self).tearDown()
        self.session.query(models.User).delete()
        self.session.query(models.FilePointer).delete()
        self.session.query(models.File).delete()
        self.session.query(models.FilePart).delete()
        self.session.commit()


class FilesServiceTests(FilesTestCase):
    def test_create_file(self):
        file = self.service.create_file(10)
        self.assertEqual(file.total_parts, 10)
        self.assertEqual(self.session.query(models.File)\
            .filter_by(id=file.id).count(), 1)

    def test_set_finish_state(self):
        file = self.service.create_file(10)
        self.service.set_file_to_finish_state(file)

        self.assertTrue(file.finished)
        self.assertEqual(self.session.query(models.File)\
            .filter_by(id=file.id, finished=True).count(), 1)

        self.assertEqual(self.session.query(models.File)\
            .filter_by(id=file.id, finished=False).count(), 0)

    def test_create_file_part(self):
        file = self.service.create_file(10)
        part = self.service.create_file_part(file, 1, b'data-1')

        self.assertEqual(part.order, 1)
        self.assertEqual(part.file_id, file.id)

        self.assertTrue(default_storage.exists(part.path))

        with default_storage.open(part.path, "rb") as f:
            self.assertEqual(f.read(), b"data-1")

    def test_create_file_pointer_for_file(self):
        file = self.service.create_file(1)
        pointer = self.service.create_file_pointer_for_file(file, "name", "meta")

        self.assertEqual(pointer.name, "name")
        self.assertEqual(pointer.meta, "meta")
        self.assertEqual(pointer.file_id, file.id)

    def test_file_parts_for_pointer(self):
        file = self.service.create_file(2)
        part1 = self.service.create_file_part(file, 1, b'data-1')
        part1 = self.service.create_file_part(file, 2, b'data-2')

        pointer = self.service.create_file_pointer_for_file(file, "name", "meta")

        parts = self.service.parts_for_pointer(pointer)
        self.assertEqual(len(parts), 2)


class DummyRequest(object):
    pass


def to_json_bytes(data):
    return to_json(data).encode('utf-8')


class FilesValidationTests(FilesTestCase):
    def test_clean_create_request_args_01(self):
        """
        Check valid create request params.
        """
        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({"total_parts": 1})

        result = files_view.clean_create_request_args(request)
        self.assertEqual(result["total_parts"], 1)

    def test_clean_create_request_args_02(self):
        """
        Check create request empty params.
        """

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({})

        with self.assertRaises(ValidateException):
            files_view.clean_create_request_args(request)

    def test_clean_create_request_args_03(self):
        """
        Check create request args with wrong type params.
        """

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({"total_parts": "k"})

        with self.assertRaises(ValidateException):
            files_view.clean_create_request_args(request)

    def test_clean_finish_request_args_01(self):
        """
        Check valid finish request params without a file.
        """

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "file_id": "1",
            "name": "name",
            "meta": "meta"
        })

        with self.assertRaises(ValidateException):
            files_view.clean_finish_request_args(request)

    def test_clean_finish_request_args_02(self):
        """
        Check valid finish request params with a file.
        """

        file = self.service.create_file(10)

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "file_id": file.id,
            "name": "name",
            "meta": "meta"
        })

        result = files_view.clean_finish_request_args(request)

    def test_clean_finish_request_args_03(self):
        """
        Check valid finish request params with a file
        in finished state.
        """

        file = self.service.create_file(10)
        self.service.set_file_to_finish_state(file)

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "file_id": file.id,
            "name": "name",
            "meta": "meta"
        })

        with self.assertRaises(ValidateException):
            files_view.clean_finish_request_args(request)

    def test_clean_finish_request_args_04(self):
        """
        Check invalid finish request (without name param).
        """

        file = self.service.create_file(10)
        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "file_id": file.id,
            "meta": "meta"
        })

        with self.assertRaises(ValidateException):
            files_view.clean_finish_request_args(request)

    def test_clean_finish_request_args_05(self):
        """
        Check invalid finish request (without meta param).
        """

        file = self.service.create_file(10)
        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "file_id": file.id,
            "name": "name"
        })

        with self.assertRaises(ValidateException):
            files_view.clean_finish_request_args(request)

    def test_clean_finish_request_args_06(self):
        """
        Check invalid finish request (without file_id param).
        """

        files_view = Files()
        request = DummyRequest()
        request.body = to_json_bytes({
            "name": "name",
            "meta": "meta"
        })

        with self.assertRaises(ValidateException):
            files_view.clean_finish_request_args(request)

    def test_clean_continue_request_args_01(self):
        """
        Check valid continue request without a existent file.
        """

        files_view = Files()
        request = DummyRequest()
        request.GET = {
            "file_id": "1",
            "order": "1",
        }

        with self.assertRaises(ValidateException):
            files_view.clean_continue_request_args(request)

    def test_clean_continue_request_args_02(self):
        """
        Check valid continue request with a existent file.
        """

        file = self.service.create_file(10)
        files_view = Files()
        request = DummyRequest()
        request.body = b"body"
        request.GET = {
            "file_id": file.id,
            "order": "1",
        }

        result = files_view.clean_continue_request_args(request)

        self.assertIn("body", result)
        self.assertIn("file", result)
        self.assertIn("order", result)

        self.assertEqual(result["file"].id, file.id)

    def test_clean_continue_request_args_03(self):
        """
        Check invalid continue request (without a file_id param)
        """

        files_view = Files()
        request = DummyRequest()
        request.body = b"body"
        request.GET = {
            "order": "1",
        }

        with self.assertRaises(ValidateException):
            files_view.clean_continue_request_args(request)

    def test_clean_continue_request_args_04(self):
        """
        Check invalid continue request (without a order param)
        """

        file = self.service.create_file(10)
        files_view = Files()
        request = DummyRequest()
        request.body = b"body"
        request.GET = {
            "file_id": file.id
        }

        with self.assertRaises(ValidateException):
            files_view.clean_continue_request_args(request)

    def test_clean_continue_request_args_05(self):
        """
        Check invalid continue request (with taken order)
        """

        file = self.service.create_file(10)
        part = self.service.create_file_part(file, 1, b"data")

        files_view = Files()
        request = DummyRequest()
        request.body = b"body"
        request.GET = {
            "file_id": file.id,
            "order": 1,
        }

        with self.assertRaises(ValidateException):
            files_view.clean_continue_request_args(request)

    def test_clean_continue_request_args_06(self):
        """
        Check invalid continue request (with file in finished state)
        """

        file = self.service.create_file(10)
        self.service.set_file_to_finish_state(file)

        files_view = Files()
        request = DummyRequest()
        request.body = b"body"
        request.GET = {
            "file_id": file.id,
            "order": 1,
        }

        with self.assertRaises(ValidateException):
            files_view.clean_continue_request_args(request)




def create_dummy_request(user, params={}, data=b""):
    request = MagicMock(name="request")
    request.user = user

    request.GET = MagicMock(spec_set=dict)
    request.GET.__getitem__.side_effect = lambda name: params[name]
    request.GET.__contains__.side_effect = lambda name: name in params

    def meta_getter(name):
        if name == "CONTENT_TYPE":
            return "application/json"
        return None

    request.META = MagicMock(spec_set=dict)
    request.META.__getitem__.side_effect = meta_getter

    if isinstance(data, dict):
        data = to_json_bytes(data)

    request.body = data
    return request


class FilesIntegrationTests(FilesTestCase):
    def test_post_create(self):
        request = create_dummy_request(self.user,
            {"cmd": "create"}, {"total_parts": 2})

        files_view = Files()
        response = files_view.post(request)

        self.assertIn(b"id", response.content)
        self.assertEqual(response.status_code, 200)

    def test_post_continue(self):
        file = self.service.create_file(10)
        request = create_dummy_request(self.user,
            {"cmd": "continue", "order": 1, "file_id": file.id}, b"data")

        files_view = Files()
        response = files_view.post(request)
        self.assertIn(b"id", response.content)
        self.assertEqual(response.status_code, 200)

    def test_post_finish(self):
        file = self.service.create_file(10)
        request = create_dummy_request(self.user, {"cmd": "finish"},
            {"name": "name", "meta":"meta", "file_id": file.id})

        files_view = Files()
        response = files_view.post(request)
        self.assertIn(b"id", response.content)
        self.assertEqual(response.status_code, 200)




