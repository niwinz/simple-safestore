# -*- coding: utf-8 -*-

import json
from django.core.urlresolvers import reverse

from djalchemy.core import sessions
from safestore.storage import models
from safestore.utils.json import to_json

from .utils import SafestoreTestCase


class ShareBlobTests(SafestoreTestCase):
    def tearDown(self):
        super(ShareBlobTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.query(models.Blob).delete()
        self.session.query(models.Contact).delete()
        self.session.commit()

    def test_share_blob_01(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")
        blob = self._create_blob(user1)

        contact = models.Contact(owner_id=user2.id, user_id=user1.id)
        self.session.add(contact)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        data = to_json({
            "parent_id": blob.id,
            "receiver_id": user2.id,
            "title": "foo",
            "content": "bar",
            "meta": "",
        })

        url = reverse('api:blob-share', args=[blob.id])

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)

    def test_share_blob_02(self):
        user1 = self._create_user(username="dd-ff")
        blob = self._create_blob(user1)

        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        data = to_json({
            "parent_id": blob.id,
            "receiver_id": "kk-dd",
            "title": "foo",
            "content": "bar",
            "meta": "",
        })

        url = reverse('api:blob-share', args=[blob.id])
        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 404)

    def test_share_blob_03(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")
        blob = self._create_blob(user1)

        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        data = to_json({
            "parent_id": blob.id,
            "title": "foo",
            "content": "bar",
            "meta": "",
        })

        url = reverse('api:blob-share', args=[blob.id])

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)

    def test_share_blob_04(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")
        blob = self._create_blob(user1)

        contact = models.Contact(owner_id=user2.id, user_id=user1.id)

        self.session.add(contact)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        data = to_json({
            "receiver_id": user2.id,
            "title": "foo",
            "content": "bar",
            "meta": "",
        })

        url = reverse('api:blob-share', args=[blob.id])

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)
