# -*- coding: utf-8 -*-

import json
from django.core import mail
from django.core.urlresolvers import reverse

from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils import crypto
from safestore.utils.json import to_json
from safestore.utils.test import patch_request_factory

from .utils import SafestoreTestCase
from .utils import DEFAULT_USERNAME
from .utils import sample_pub_key

patch_request_factory()


class UserTests(SafestoreTestCase):
    def tearDown(self):
        super(UserTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.commit()

    def test_register(self):
        url = reverse('api:user')
        data = to_json({
            "username": "foousername",
            "email": "niwi@niwi.es",
            "password": "123123",
            "pubkey": sample_pub_key,
        })

        response = self.client.post(url, data=data,
            content_type="application/json", follow=False)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.content.decode('utf-8'))
        self.assertIn("id", data)

    def test_invalid_register_01(self):
        url = reverse('api:user')
        data = to_json({"email": "niwi@niwi.be"})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 400)

    def test_invalid_register_02(self):
        url = reverse('api:user')
        data = to_json({"password": "1111111"})

        response = self.client.post(url, data=data,
                        content_type="application/json")
        self.assertEqual(response.status_code, 400)

    def test_invalid_register_03(self):
        url = reverse('api:user')
        data = to_json({"email": "niwi@niwi.be",
                            "password": "1111111"})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 400)

    def test_invalid_register_04(self):
        url = reverse('api:user')
        data = to_json({"email": "niwi@niwi.be",
                            "password": "1111111"})

        response = self.client.post(url, data=data,
                        content_type="application/data")

        self.assertEqual(response.content, b"Wrong content type. Must be 'application/json'.")
        self.assertEqual(response.status_code, 400)

    def test_invalid_register_05(self):
        url = reverse('api:user')
        response = self.client.post(url, data="",
                        content_type="application/json")

        self.assertEqual(response.content, b"Empty body is not permited")
        self.assertEqual(response.status_code, 400)

    def test_unregister(self):
        user = self._create_user()
        self.session.commit()
        authkwargs, token = self._login()

        url = reverse("api:user")
        response = self.client.delete(url, content_type="application/json",
                                                                **authkwargs)
        self.assertEqual(response.status_code, 204)

    def test_modify_existing_user(self):
        user = self._create_user()
        self.session.commit()
        authkwargs, token = self._login()

        url = reverse("api:user")
        data = to_json({"password": "123123", "pubkey":"aaa"})
        response = self.client.patch(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)

    def test_activate_user_01(self):
        user = self._create_user(is_active=False)
        user.token = crypto.generate_new_token()

        self.session.add(user)
        self.session.commit()

        url = reverse('api:user-activation')

        data = to_json({"username": DEFAULT_USERNAME,
                        "token": user.token})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 204)

    def test_activate_user_02(self):
        user = self._create_user(is_active=True)
        user.token = crypto.generate_new_token()

        self.session.add(user)
        self.session.commit()

        url = reverse('api:user-activation')

        data = to_json({"username": DEFAULT_USERNAME,
                        "token": user.token})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 400)

    def test_activate_user_03(self):
        user = self._create_user(is_active=True)
        user.token = crypto.generate_new_token()

        self.session.add(user)
        self.session.commit()

        url = reverse('api:user-activation')

        data = to_json({"username-foo": DEFAULT_USERNAME,
                        "token": user.token})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 400)

    def test_activate_user_04(self):
        user = self._create_user(is_active=True)
        user.token = crypto.generate_new_token()

        self.session.add(user)
        self.session.commit()

        url = reverse('api:user-activation')
        data = to_json({"username": DEFAULT_USERNAME,
                        "token-foo": user.token})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        self.assertEqual(response.status_code, 400)

    def test_profile(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()

        url = reverse('api:profile')
        response = self.client.get(url, **authkwargs)
        data = json.loads(response.content.decode('utf-8'))

        self.assertIn("node_name", data)

    def test_user(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()
        url = reverse('api:user')

        response = self.client.get(url, **authkwargs)
        self.assertEqual(response.status_code, 200)

    def test_user_as_str(self):
        user = self._create_user()
        self.assertEqual(str(user), user.id)
