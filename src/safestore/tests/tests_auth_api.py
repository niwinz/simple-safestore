# -*- coding: utf-8 -*-

import json

from django.core.urlresolvers import reverse
from django.core.cache import cache

from safestore.storage import models
from djalchemy.core import sessions

from safestore.utils.json import to_json

from .utils import SafestoreTestCase
from .utils import DEFAULT_USERNAME


class AuthTests(SafestoreTestCase):
    def tearDown(self):
        super(AuthTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.commit()

    def test_invalid_login_01(self):
        url = reverse('api:login')
        data = to_json({"username": DEFAULT_USERNAME})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        data = json.loads(response.content.decode('utf-8'))
        self.assertIn("errors", data)

    def test_invalid_login_02(self):
        url = reverse('api:login')
        data = to_json({"password": "123444"})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        data = json.loads(response.content.decode('utf-8'))
        self.assertIn("errors", data)

    def test_invalid_login_03(self):
        url = reverse('api:login')
        data = to_json({"username": DEFAULT_USERNAME, "password": "123444"})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        data = json.loads(response.content.decode('utf-8'))
        self.assertIn("errors", data)

    def test_invalid_login_04(self):
        url = reverse('api:login')
        user = self._create_user()

        self.session.commit()

        data = to_json({"username": DEFAULT_USERNAME, "password": "123444"})
        response = self.client.post(url, data=data,
                        content_type="application/json")

        data = json.loads(response.content.decode('utf-8'))

    def test_invalid_auth(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()
        cache.set(token, 'ddd-ddd-ddd')

        url = reverse('api:user')
        response = self.client.get(url, {}, **authkwargs)
        self.assertEqual(response.status_code, 401)
