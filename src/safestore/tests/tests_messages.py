# -*- coding: utf-8 -*-

import json

from django.core.urlresolvers import reverse
from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils import crypto
from safestore.utils.json import to_json

from .utils import SafestoreTestCase


class MessagesTests(SafestoreTestCase):
    def tearDown(self):
        super(MessagesTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.query(models.Message).delete()
        self.session.commit()

    def test_get_messages(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")

        message = models.Message(recipient_id=user1.id, owner_id=user2.id,
                                                content="foo", meta="bar")
        self.session.add(message)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        response = self.client.get(url, **authkwargs)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(len(data), 1)

    def test_send_message_00(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")

        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to_id": user2.id,
            "meta": "BAR",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)

    def test_send_message_01(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")
        user2.only_contacts = False

        contact = models.Contact(owner_id=user1.id, user_id=user2.id)

        self.session.add(contact)
        self.session.add(user2)
        self.session.commit()

        authkwargs, token = self._login( username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to": contact.id,
            "content": "FOOO",
            "meta": "BAR",
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)
        self.assertEqual(self.session.query(models.Message).count(), 1)

    def test_send_message_02(self):
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")
        user2.only_contacts = False

        contact = models.Contact(owner_id=user1.id, user_id=user2.id)
        message = models.Message(recipient_id=user1.id, owner_id=user2.id,
                                                content="foo", meta="bar")
        self.session.add(user2)
        self.session.add(contact)
        self.session.add(message)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to": contact.id,
            "content": "FOOO",
            "meta": "BAR",
            "reply_to": message.id,
            "content_self": "FOOO",
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)
        self.assertEqual(self.session.query(models.Message).count(), 2)

    def test_send_message_03(self):
        """
        Test reply to unexistent message.
        """

        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")

        contact = models.Contact(owner_id=user1.id, user_id=user2.id)
        message = models.Message(recipient_id=user1.id, owner_id=user2.id,
                                                content="foo", meta="bar")
        user2.only_contacts = False
        self.session.add(contact)
        self.session.add(user2)
        self.session.add(message)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to": contact.id,
            "content": "FOOO",
            "meta": "BAR",
            "reply_to": message.id + "ddd",
            "content_self": "FOOO",
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)


        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.session.query(models.Message).count(), 1)

    def test_send_message_04(self):
        """
        Test reply to message without content_self
        """

        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")

        contact = models.Contact(owner_id=user1.id, user_id=user2.id)
        message = models.Message(recipient_id=user1.id, owner_id=user2.id,
                                                content="foo", meta="bar")
        user2.only_contacts = False
        self.session.add(contact)
        self.session.add(user2)
        self.session.add(message)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to": contact.id,
            "content": "FOOO",
            "meta": "BAR",
            "reply_to": message.id,
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.session.query(models.Message).count(), 1)

    def test_send_message_05(self):
        """
        Test send message to user that not accepts messages
        from incoming messages from not contacts.
        """
        user1 = self._create_user(username="dd-ff")
        user2 = self._create_user(username="dd-fd", email="niwi@niwi.es")

        contact = models.Contact(owner_id=user1.id, user_id=user2.id)
        self.session.add(contact)
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to": contact.id,
            "content": "FOOO",
            "meta": "BAR",
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.session.query(models.Message).count(), 0)

    def test_send_message_06(self):
        """
        Test send message to unexistent contact.
        """

        user1 = self._create_user(username="dd-ff")
        self.session.commit()

        authkwargs, token = self._login(username="dd-ff")

        url = reverse('api:messages')
        data = to_json({
            "to_id": "kk-dd-ff-gg",
            "content": "FOOO",
            "meta": "BAR",
            "subject": "subject",
        })

        response = self.client.put(url, data=data,
            content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.session.query(models.Message).count(), 0)
