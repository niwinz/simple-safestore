# -*- coding: utf-8 -*-

import json
import unittest

from django.conf import settings
from safestore.utils import crypto


class CryptoTests(unittest.TestCase):
    def test_internal_random_iv(self):
        iv = crypto._generate_random_iv()
        self.assertIsInstance(iv, bytes)
        self.assertEqual(len(iv), 8)

    def test_internal_server_key(self):
        key = crypto._get_server_key()
        self.assertEqual(key, settings.SECRET_KEY)

    def test_internal_combined_key(self):
        key = crypto._get_combined_key("sample-password")
        self.assertIn("$", key)
        self.assertEqual(key.split("$")[0], settings.SECRET_KEY[:10])

    def test_internal_encrypt(self):
        data = crypto._encrypt("sample-key", b"1"*8, "sample-data-áê".encode('utf-8'))

        self.assertIsInstance(data, bytes)
        self.assertEqual(data, b"b8215ba15d8548fb3072c37bf3ca7664")

    def test_internal_decrypt(self):
        data = crypto._decrypt("sample-key", b"1"*8, b"b8215ba15d8548fb3072c37bf3ca7664")
        self.assertIsInstance(data, bytes)
        self.assertEqual(data, "sample-data-áê".encode('utf-8'))

    def test_encrypt_decrypt_bytes(self):
        data = crypto.b_encrypt("sample-key", bytes("sample-data-áê", "utf-8"))
        self.assertIsInstance(data, bytes)

        data = crypto.b_decrypt("sample-key", data)
        self.assertIsInstance(data, bytes)
        self.assertEqual(data, bytes("sample-data-áê", "utf-8"))

    def test_encrypt_decrypt_unicode(self):
        data = crypto.encrypt("sample-key", "sample-data-áê")
        self.assertIsInstance(data, str)

        data = crypto.decrypt("sample-key", data)
        self.assertIsInstance(data, str)
        self.assertEqual(data, "sample-data-áê")

    def test_encrypt_decrypt_with_combined_key(self):
        data = crypto.encrypt_with_combined_key("sample-password", "sample-data-áê")
        self.assertIsInstance(data, str)

        data = crypto.decrypt_with_combined_key("sample-password", data)
        self.assertIsInstance(data, str)
        self.assertEqual(data, "sample-data-áê")

    def test_encrypt_decrypt_with_server_key(self):
        data = crypto.encrypt_with_server_key("sample-data-áê")
        self.assertIsInstance(data, str)

        data = crypto.decrypt_with_server_key(data)
        self.assertIsInstance(data, str)
        self.assertEqual(data, "sample-data-áê")

    def test_new_token(self):
        token = crypto.generate_new_token()
        self.assertIsInstance(token, str)
        self.assertEqual(len(token), 64)

    def test_get_raw_pubkey(self):
        crypto._privkey_cache = None
        crypto._pubkey_cache = {}

        key = crypto.get_raw_pubkey()
        key = crypto.get_raw_pubkey()
        self.assertNotEqual(key, None)
        self.assertIn("self", crypto._pubkey_cache)

        key = crypto.get_raw_pubkey(node="nodes-eu01.safestoreproject.co.nz")
        self.assertIn("nodes-eu01.safestoreproject.co.nz", crypto._pubkey_cache)

    def test_get_raw_privkey(self):
        key = crypto.get_raw_privkey()
        key = crypto.get_raw_privkey()
        self.assertNotEqual(crypto._privkey_cache, None)

    def test_rsa_encryption_01(self):
        data = crypto.encrypt_with_pubkey("self", "hello")
        data = crypto.decrypt_with_privkey(data)
        self.assertEqual(data, "hello")

    def test_rsa_encryption_02(self):
        data = crypto.encrypt_with_pubkey("nodes-eu01.safestoreproject.co.nz", "hello")
        data = crypto.decrypt_with_privkey(data)
        self.assertEqual(data, "hello")
