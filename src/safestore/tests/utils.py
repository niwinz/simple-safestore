# -*- coding: utf-8 -*-

import json
import uuid
import hashlib
import unittest

from django.core.urlresolvers import reverse
from django.test.client import Client

from djalchemy.core import engines, sessions
from djalchemy.declarative import BaseModel

from safestore.middleware.auth import SAFESTORE_TOKEN_HEADER
from safestore.utils.json import to_json
from safestore.storage import models


sample_pub_key = """-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDKbcusrGHMGbrlIk8DEcEPNguj
DFM+zsqn+ypTmcc/sAs8KbsLwhY14x/A8F2ooV6DTbKmgk6WwG4m+r5tMjlwqYKs
iGKEpwezT10ljtBLLqqfG/2bYUHOK2bn2+6sJFlDMfbh+KNYP+SLOoHAsWsGZzsQ
s6uSiyc0UCer6qgdzQIDAQAB
-----END PUBLIC KEY-----"""


DEFAULT_USERNAME = '118D5E98-3F2E-11E2-BD40-0024BEB1B00A'.lower()


class SafestoreTestCase(unittest.TestCase):
    #@classmethod
    #def setUpClass(cls):
    #    BaseModel.metadata.create_all(engines["default"])

    #@classmethod
    #def tearDownClass(cls):
    #    BaseModel.metadata.drop_all(engines["default"])

    def setUp(self):
        self.session = sessions.get_session()
        self.client = Client()

    def tearDown(self):
        self.session.rollback()
        sessions.remove_scoped_all()

    def _create_user(self, username=DEFAULT_USERNAME, email="niwi@niwi.be",
                                    password="123123", is_active=True):
        user = models.User(username=username, pubkey=sample_pub_key)
        user.id = str(uuid.uuid1())
        user.email_hash = hashlib.sha256(email.encode('utf-8')).hexdigest()
        user.set_password(password)
        user.is_active = is_active

        self.session.add(user)
        return user

    def _login(self, username=DEFAULT_USERNAME, password="123123"):
        url = reverse('api:login')
        data = to_json({"username": username, "password": password})

        response = self.client.post(url, data=data,
                        content_type="application/json")

        data = json.loads(response.content.decode('utf-8'))
        authkwargs = {SAFESTORE_TOKEN_HEADER:data["token"]}
        token = data["token"]
        return authkwargs, token

    def _create_blob(self, user):
        data = "123412341234"

        blob = models.Blob(
            title="Foo",
            content = data,
            owner_id = user.id,
            accepted = True
        )

        self.session.add(blob)
        return blob

    def _create_contact(self, user, id="ddd-ddd-ddd", alias="", domain=""):
        contact = models.Contact(
            owner_id = user.id,
            user_id = id,
            alias = alias,
            domain = domain,
        )
        self.session.add(contact)
        return contact
