# -*- coding: utf-8 -*-

import json
from django.core.urlresolvers import reverse

from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils.json import to_json
from safestore.utils.test import patch_request_factory

from .utils import SafestoreTestCase


patch_request_factory()


class ContactTests(SafestoreTestCase):
    def tearDown(self):
        super(ContactTests, self).tearDown()
        self.session.query(models.User).delete()
        self.session.query(models.Contact).delete()
        self.session.commit()

    def test_create_contact_01(self):
        user = self._create_user()
        user2 = self._create_user(username="dd-ff-gg", email="niwi@niwi.es")

        self.session.commit()

        authkwargs, token = self._login()

        url = reverse('api:contacts')
        data = to_json({"user_id": user2.id, "alias": "kk", "meta": "dd"})

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)
        self.assertIn(b"id", response.content)

    def test_create_contact_02(self):
        user = self._create_user()
        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:contacts')
        data = to_json({"idd": "ddd-ddd-ddd", "alias": "kk", "meta":"dd"})

        response = self.client.post(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 400)

    def test_access_to_contact_list(self):
        user = self._create_user()
        contact = self._create_contact(user)

        self.session.commit()

        authkwargs, token = self._login()

        url = reverse('api:contacts')
        response = self.client.get(url, **authkwargs)

        self.assertEqual(response.status_code, 200)
        obj = json.loads(response.content.decode('utf-8'))

        self.assertEqual(len(obj), 1)

    def test_modify_existent_contact_01(self):
        user = self._create_user()
        contact = self._create_contact(user)

        self.session.commit()

        authkwargs, token = self._login()
        url = reverse('api:contact', args=[contact.id])

        data = to_json({"alias": "Fooo"})
        response = self.client.patch(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 200)

        obj = json.loads(response.content.decode('utf-8'))
        self.assertIn('alias', obj)
        self.assertEqual(obj['alias'], 'Fooo')

    def test_modify_existent_contact_02(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()

        url = reverse('api:contact', args=["ddd-ddd-ddd"])
        data = to_json({"alias": "Fooo"})

        response = self.client.patch(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 404)

    def test_delete_contacts_01(self):
        user = self._create_user()
        contact = self._create_contact( user)
        self.session.commit()

        authkwargs, token = self._login()
        data = to_json({"contacts": [contact.id]})

        url = reverse('api:contacts')
        response = self.client.delete(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 204)

    def test_delete_contacts_02(self):
        user1 = self._create_user(username="ff-ff")
        user2 = self._create_user(username="ff-dd", email="niwi@niwi.es")

        contact1 = self._create_contact(user1, id="ddd-ddd-fff")
        contact2 = self._create_contact(user2, id="ddd-fff-fff")

        self.session.commit()

        authkwargs, token = self._login(username="ff-ff")
        data = to_json({"contacts": [contact2.id]})

        url = reverse('api:contacts')
        response = self.client.delete(url, data=data,
                    content_type="application/json", **authkwargs)

        self.assertEqual(response.status_code, 404)

    def test_delete_one_contact_01(self):
        user = self._create_user()
        contact = self._create_contact(user)

        self.session.commit()
        authkwargs, token = self._login()

        url = reverse('api:contact', args=[contact.id])
        response = self.client.delete(url, **authkwargs)

    def test_delete_one_contact_02(self):
        user = self._create_user()
        self.session.commit()

        authkwargs, token = self._login()
        url = reverse('api:contact', args=["ddd-ddd-ddd"])
        response = self.client.delete(url, **authkwargs)
        self.assertEqual(response.status_code, 404)
