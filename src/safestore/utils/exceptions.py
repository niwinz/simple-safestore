# -*- coding: utf-8 -*-


class ValidateException(Exception):
    pass


class NotFoundException(Exception):
    pass


