# -*- coding: utf-8 -*-

from django.conf import settings
from django.utils.crypto import get_random_string

from Crypto.PublicKey import RSA
from Crypto.Cipher import Blowfish
from Crypto import Random

import binascii
import random
import hashlib
import json
import io


def _encrypt(key, iv, data):
    """
    Basic encrypt method.
    """
    bf = Blowfish.new(key, Blowfish.MODE_CFB, iv)
    encrypted = bf.encrypt(data)
    return binascii.hexlify(encrypted)


def _decrypt(key, iv, data):
    """
    Basic decrypt method.
    """
    bdata = binascii.unhexlify(data)
    bf = Blowfish.new(key, Blowfish.MODE_CFB, iv)
    return bf.decrypt(bdata)


def _generate_random_iv():
    """
    Basic method for generate a random 8 bytes iv.
    """
    return bytes(get_random_string(8), 'utf-8')


def _get_server_key():
    return settings.SECRET_KEY


def _get_combined_key(password):
    """
    Combine server key with user password.
    This method used on login/register process
    for safe store user mail.
    """
    server_key = _get_server_key()
    key_hash = hashlib.sha256(password.encode('utf-8')).hexdigest()
    return "{0}${1}".format(server_key[:10], key_hash[:10])


def b_encrypt(key, data):
    iv = _generate_random_iv()
    enc_data = _encrypt(key, iv, data)
    return b"$".join([b"bf", iv, enc_data])


def b_decrypt(key, data):
    enc, iv, encdata = data.split(b"$")
    return _decrypt(key, iv, encdata)


def encrypt(key, data):
    return b_encrypt(key, data.encode('utf-8')).decode('utf-8')


def decrypt(key, data):
    return b_decrypt(key, data.encode('utf-8')).decode('utf-8')


def encrypt_with_combined_key(password, data):
    """
    Encript data with combined key.
    Used for safe store email of user on adatabase
    and only can be recovered on login process.
    """
    key = _get_combined_key(password)
    return encrypt(key, data)


def decrypt_with_combined_key(password, data):
    """
    Decript data with combined key.
    Used for safe store email of user on adatabase
    and only can be recovered on login process.
    """

    key = _get_combined_key(password)
    return decrypt(key, data)


def encrypt_with_server_key(data):
    key = _get_server_key()
    return encrypt(key, data)


def decrypt_with_server_key(data):
    key = _get_server_key()
    return decrypt(key, data)


def generate_new_token():
    rndstr = get_random_string(10000)
    return hashlib.sha256(rndstr.encode('utf-8')).hexdigest()


_privkey_cache = None
_pubkey_cache = {}


def get_raw_pubkey(node='self'):
    global _pubkey_cache

    if node in _pubkey_cache:
        return _pubkey_cache[node]

    if node == 'self':
        keypath = settings.SAFESTORE_PUBKEY
    else:
        keypath = settings.SAFESTORE_TRUSTED_NODES[node]

    with io.open(keypath, "rb") as keyfile:
        _pubkey_cache[node] = keyfile.read()

    return _pubkey_cache[node]


def get_raw_privkey():
    global _privkey_cache

    if _privkey_cache is None:
        with io.open(settings.SAFESTORE_PRIVKEY, 'rb') as keyfile:
            _privkey_cache = keyfile.read()

    return _privkey_cache


def encrypt_with_pubkey(nodename, data):
    pubkey = get_raw_pubkey(node=nodename)
    rsa = RSA.importKey(pubkey)

    raw_meta_key = bytes(get_random_string(20), 'utf-8')
    crypted_data = encrypt(raw_meta_key, data)
    encrypted_meta_key = rsa.encrypt(raw_meta_key, raw_meta_key)[0]

    return json.dumps({
        "data": crypted_data,
        "meta": binascii.hexlify(encrypted_meta_key).decode('utf-8')
    })


def decrypt_with_privkey(data):
    res = json.loads(data)

    encrypted_meta_key = binascii.unhexlify(res['meta'])
    encrypted_data = res['data']

    privkey = get_raw_privkey()
    rsa = RSA.importKey(privkey)

    raw_meta_key = rsa.decrypt(encrypted_meta_key)
    return decrypt(raw_meta_key, encrypted_data)
