# -*- coding: utf-8 -*-

from django.views.generic import View

class View(View):
    http_method_names = ['get', 'post', 'put', 'delete', 'head', 'options', 'trace', 'patch']

__all__ = ['View']
