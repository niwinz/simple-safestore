# -*- coding: utf-8 -*-

import functools

from safestore.utils import http
from safestore.utils.json import request_json_to_dict
from safestore.utils.exceptions import ValidateException, NotFoundException


def user_required(method):
    @functools.wraps(method)
    def _decorator(self, request, *args, **kwargs):
        if request.user is None or not request.user.is_active:
            return http.HttpUnauthorized("Unauthenticaded request.")

        return method(self, request, *args, **kwargs)
    return _decorator


def request_body_required(method):
    @functools.wraps(method)
    def _decorator(self, request, *args, **kwargs):
        if not request.body:
            return http.HttpBadRequest("Empty body is not permited")
        return method(self, request, *args, **kwargs)
    return _decorator


def json_contenttype_required(method):
    @functools.wraps(method)
    def _decorator(self, request, *args, **kwargs):
        content_type = request.META['CONTENT_TYPE']
        if not content_type.startswith('application/json'):
            return http.HttpBadRequest("Wrong content type. Must be 'application/json'.")

        request.data = request_json_to_dict(request)
        return method(self, request, *args, **kwargs)
    return _decorator


def validation_intercept(method):
    @functools.wraps(method)
    def _decorator(self, *args, **kwargs):
        try:
            return method(self, *args, **kwargs)
        except ValidateException as e:
            return http.HttpBadRequest(str(e))
        except NotFoundException as e:
            return http.HttpNotFound(str(e))

    return _decorator


def apply_validator(validator_cls):
    def _decorator(method):
        @functools.wraps(method)
        def _wrapper(self, request, *args, **kwargs):
            instance = validator_cls(request)
            instance.validate()
            return method(self, request, *args, **kwargs)
        return _wrapper
    return _decorator
