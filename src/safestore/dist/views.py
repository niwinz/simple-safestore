# -*- coding: utf-8 -*-

import uuid
import json
import hashlib
import functools

from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils import formats

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func
from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils import crypto
from safestore.utils.views import generic, decorators
from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils import http

# Services imports
from safestore.api.views.messages import MessagesService


def parse_trustring_request(method):
    class FakeUser(object):
        def __init__(self, id):
            self.id = id

    @functools.wraps(method)
    def _decorator(self, request, *args, **kwargs):
        data = request_json_to_dict(request)

        if "user" not in data and "body" not in data:
            return http.HttpBadRequest("Missing fields")

        request.user = FakeUser(data['user'])
        request.body = crypto.decrypt_with_privkey(data['body'])

        return method(self, request, *args, **kwargs)

    return _decorator


class Messages(generic.View):
    """
    View thar receives trust messages from other
    nodes of a trust ring.
    """

    @parse_trustring_request
    @decorators.validation_intercept
    def put(self, request):
        body = request_json_to_dict(request)

        service = MessagesService()
        service.create(body, request.user)
        return http.HttpNoContent()
