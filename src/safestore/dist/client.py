# -*- coding: utf-8 -*-

import json
import requests

from django.core.urlresolvers import reverse
from django.conf import settings
from superview.utils import to_json

from safestore.utils import crypto


def send_message(node, userid, body, client=None):
    if not client:
        client = requests

    data = to_json({
        "body": crypto.encrypt_with_pubkey(node, to_json(body)),
        "user": userid,
    })

    response = client.put(url, data=data)
    return response.status_code < 300


def check_user_exists_in_node(node, user_id, client=None)
    if not client:
        client = requests

    url = reverse('api:user')
    response = client.head(url, {"username": user_id})
    return response.status_code < 300
