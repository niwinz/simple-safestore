# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from safestore.storage import models
from safestore.utils.exceptions import ValidateException


class UserRegisterValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        data = self.request.data

        for field in ["email", "password", "pubkey", "username"]:
            if field not in data:
                raise ValidateException("'{}' field is mandatory".format(field))

        session = sessions.get_scoped_session()
        if session.query(models.User).filter_by(id=data["username"].lower()).count() > 0:
            raise ValidateException("username is taken")
