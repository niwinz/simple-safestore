# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from safestore.storage import models
from safestore.utils.exceptions import ValidateException, NotFoundException


class BlobValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        required_fields = ["title", "content", "meta"];

        for field in required_fields:
            if field not in self.request.data:
                raise ValidateException("'{}' field is mandatory".format(field))


class BlobShareValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        if "receiver_id" not in self.request.data:
            raise ValidateException("'receiver_id' field is mandatory")

        session = sessions.get_scoped_session()
        contact_query = session.query(models.Contact).filter(
                    models.Contact.owner_id == self.request.user.id,
                    models.Contact.id == self.request.data['receiver_id'].lower())

        try:
            contact = contact_query.one()
        except NoResultFound:
            raise NotFoundException("Receiver contact does not exists")

        if contact.is_local:
            user_query = session.query(models.User).filter(
                                models.User.id == contact.user_id)

            contact_query = session.query(models.Contact).filter(
                                models.Contact.owner_id == contact.user_id,
                                models.Contact.user_id == self.request.user.id)

            try:
                user = user_query.one()
            except NoResultFound:
                raise NotFoundException("receiver user does not exists")
            else:
                if user.only_contacts and contact_query.count() == 0:
                    raise ValidateException("Receiver user does not have your as contact")
