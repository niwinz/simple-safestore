# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from safestore.storage import models
from safestore.utils.exceptions import ValidateException


class ContactsValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        required_fields = ["user_id", "alias", "meta"]

        for field in required_fields:
            if field not in self.request.data:
                raise ValidateException("'{}' field is mandatory".format(field))


class ContactsDeleteValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        if "contacts" not in self.request.data:
            raise ValidateException("contacts field is mandatory")


