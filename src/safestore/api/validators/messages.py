# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from safestore.storage import models
from safestore.utils.exceptions import ValidateException


class MessagesValidator(object):
    def __init__(self, request):
        self.request = request

    def validate(self):
        self.validate_fields(self.request.data)
        self.validate_permissions(self.request.data)
        self.validate_reply_to(self.request.data)

    def validate_fields(self, body):
        for field in ["to", "content", "subject", "meta"]:
            if field not  in body:
                raise ValidateException("'{}' field is mandatory".format(field))

    def validate_permissions(self, body):
        """
        Validates the existence of recipient contact.
        """
        session = sessions.get_scoped_session()
        recipient_id = body['to'].lower()
        try:
            recipient = session.query(models.Contact).filter(
                                models.Contact.owner_id == self.request.user.id,
                                models.Contact.id == recipient_id).one()
        except NoResultFound:
            raise ValidateException("to refers to not existent contact")

        # If contact user is locally exists,
        # test if it accepts a message.
        try:
            recipient_user = session.query(models.User).filter(
                    models.User.id == recipient.user_id).one()

            if recipient_user.only_contacts:
                query = session.query(models.Contact).filter(
                            models.Contact.owner_id == recipient_user.id,
                            models.Contact.user_id == self.request.user.id)

                if not session.query(exists(query.statement)).scalar():
                    raise ValidateException("the contact only accepts messages"
                                            " from known contacts")
        except NoResultFound:
            pass


    def validate_reply_to(self, body):
        if "reply_to" not in body:
            return None

        if "content_self" not in body:
            raise ValidateException("on reply messages 'content_self' field is mandatory")

        session = sessions.get_scoped_session()

        reply_to = body['reply_to'].lower()
        query = session.query(models.Message).filter(
                    models.Message.id == reply_to,
                    models.Message.recipient_id == self.request.user.id)

        if query.count() == 0:
            raise ValidateException("reply_to refers to inexistent message")
