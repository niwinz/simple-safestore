from django.conf.urls import patterns, include, url

from .views import blob as blob_views
from .views import auth as auth_views
from .views import user as user_views
from .views import contact as contact_views
from .views import share as share_views
from .views import messages as messages_view


urlpatterns = patterns('',
    url(r'^login$', auth_views.Login.as_view(), name='login'),
    url(r'^user$', user_views.User.as_view(), name='user'),
    url(r'^user/activation$', user_views.Activation.as_view(), name='user-activation'),

    url(r'^blobs$', blob_views.Blobs.as_view(), name='blobs'),
    url(r'^blobs/(?P<id>[\w\d\-]+)$', blob_views.Blob.as_view(), name='blob'),
    url(r'^blobs/(?P<id>[\w\d\-]+)/shares',
                    share_views.ShareBlob.as_view(),
                    name = 'blob-share'),

    url(r'^contacts$', contact_views.Contacts.as_view(), name='contacts'),
    url(r'^contacts/(?P<id>[\w\d\-]+)$',
                    contact_views.Contact.as_view(),
                    name='contact'),

    url(r'^profile$', user_views.Profile.as_view(), name='profile'),
    url(r'^messages$', messages_view.Messages.as_view(), name='messages'),
    url(r'^messages/(?P<id>[\w\d\-]+)$', messages_view.Message.as_view(), name='message'),
)
