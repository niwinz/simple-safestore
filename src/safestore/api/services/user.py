# -*- coding: utf-8 -*-

import hashlib

from django.conf import settings
from django.template.loader import render_to_string

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy import func
from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils.exceptions import ValidateException
from safestore.utils import crypto as crypto_utils


def user_to_dict(user):
    return {
        "id": user.id,
        "username": user.username,
        "pubkey": user.pubkey,
        "only_contacts": user.only_contacts,
        "blacklist": user.blacklist,
    }


class UsersService(object):
    def create(self, data):
        user = models.User(pubkey=data["pubkey"].strip())
        user.username = data['username']
        user.email_hash = hashlib.sha256(data['email'].encode('utf-8')).hexdigest()
        user.set_password(data['password'])
        user.token = crypto_utils.generate_new_token()
        user.is_active = False

        session = sessions.get_scoped_session()
        try:
            session.add(user)
            session.commit()
            return user
        except IntegrityError as e:
            session.rollback()
            raise ValidateException("current email is taken")

    def send_verification_mail(self, user, email):
        from django.core.mail import send_mail
        tmpl = render_to_string("verification-mail.jinja", {'user':user})
        send_mail("Safestore: account verification", tmpl,
            settings.DEFAULT_FROM_EMAIL, [email])

    def update_by_id(self, userid, data):
        password = data.pop("password", None)

        valid_attrs = set(["pubkey", "only_contacts", "blacklist"])
        if set(data.keys()).difference(valid_attrs):
            raise ValidateException("Invalid parameters")

        session = sessions.get_scoped_session()
        session.query(models.User).filter_by(id=userid)\
                    .update(data, synchronize_session=False)
        session.commit()

    def exists_by_username(self, id):
        session = sessions.get_scoped_session()

        if session.query(models.User).filter(
                        models.User.id == id).count() == 0:
            return False
        return True

    def get_by_username(self, username):
        session = sessions.get_scoped_session()

        try:
            user = session.query(models.User).filter(
                    models.User.username == username).one()
            return user
        except NoResultFound:
            raise ValidateException("User does not exists")

    def get_by_id(self, id):
        session = sessions.get_scoped_session()

        try:
            user = session.query(models.User).filter(
                                models.User.id == id).one()
            return user
        except NoResultFound:
            raise ValidateException("User does not exists")

    def delete_by_id(self, id):
        session = sessions.get_scoped_session()
        session.query(models.Blob).filter_by(owner_id=id).delete()
        session.query(models.Contact).filter_by(owner_id=id).delete()
        session.query(models.User).filter_by(id=id)\
                    .delete(synchronize_session=False)
        session.commit()

    def verify(self, token):
        session = sessions.get_scoped_session()
        try:
            user = session.query(models.User)\
                .filter_by(token=token, is_active=False).one()

            user.is_active = True
            session.add(user)
            session.commit()
            return user
        except NoResultFound:
            raise ValidateException("Token does not exists")
