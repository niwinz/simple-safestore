# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from djalchemy.transaction import transaction_method

from safestore.storage import models
from safestore.utils.exceptions import ValidateException, NotFoundException


def create_contact_dict(contact):
    return {
        'id': contact.id,
        'owner_id': contact.owner_id,
        'user_id': contact.user_id,
        'pubkey': contact.pubkey,
        'alias': contact.alias,
        'meta': contact.meta
    }


class ContactsService(object):
    def __init__(self, user):
        self.user = user

    def get(self, id):
        session = sessions.get_scoped_session()

        try:
            contact = session.query(models.Contact).filter(
                    models.Contact.owner_id == self.user.id,
                    models.Contact.id == id).one()
            return contact

        except NoResultFound:
            raise ValidateException("Contact does not exist")

    def all(self):
        s = sessions.get_scoped_session()
        query = s.query(models.Contact).filter(
                    models.Contact.owner_id == self.user.id)

        return query.all()

    @transaction_method
    def create(self, session, data):
        user_id = data["user_id"]
        query = session.query(models.User).filter(
                    models.User.id == user_id)

        try:
            contact_user = query.one()
        except NoResultFound:
            raise ValidateException("User does not exist")

        contact = models.Contact()
        contact.owner_id = self.user.id
        contact.pubkey = contact_user.pubkey
        contact.user_id = contact_user.id
        contact.alias = data['alias']
        contact.meta = data['meta']
        contact.domain = data.get('domain', '')

        session.add(contact)
        return contact

    @transaction_method
    def delete_many_by_id(self, session, contacts):
        query = session.query(models.Contact).filter(
            models.Contact.id.in_(contacts),
            models.Contact.owner_id == self.user.id)

        if query.count() != len(contacts):
            raise NotFoundException("Unexpected error")

        query.delete(synchronize_session=False)

    @transaction_method
    def update_by_id(self, session, contactid, data):
        query = session.query(models.Contact).filter(
                    models.Contact.id == contactid,
                    models.Contact.owner_id == self.user.id)
        try:
            contact = query.one()
        except NoResultFound:
            raise NotFoundException("contact not found")

        if "alias" in data:
            contact.alias = data['alias']

        if "domain" in data:
            contact.domain = data['domain']

        if "meta" in data:
            contact.meta = data['meta']

        session.add(contact)
        return contact
