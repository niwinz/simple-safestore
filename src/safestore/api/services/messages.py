# -*- coding: utf-8 -*-

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from djalchemy.transaction import transaction_method

from safestore.storage import models
from safestore.utils.exceptions import ValidateException, NotFoundException

from .contact import ContactsService
from .user import UsersService


def message_to_dict(obj):
    return {
        "id": obj.id,
        "owner_id": obj.owner_id,
        "recipient_id": obj.recipient_id,
        "subject": obj.subject,
        "content": obj.content,
        "response": obj.response,
        "created_at": obj.created_at.isoformat(),
        "meta": obj.meta,
    }


class MessagesService(object):
    def __init__(self, user):
        self.user = user


    def update_with_reply_to(self, message, to, content):
        session = sessions.get_scoped_session()

        reply_to = to.lower()
        message.reply_to_id = reply_to

        session.query(models.Message).filter_by(id=reply_to)\
            .update({"response": content}, synchronize_session=False)

    def new(self, recipient, subject, content, meta):
        message = models.Message(owner_id=self.user.id, recipient_id=recipient.id)
        message.subject = subject
        message.meta = meta
        message.content = content

        session = sessions.get_scoped_session()
        session.add(message)
        return message


    def all(self):
        session = sessions.get_scoped_session()
        messages = session.query(models.Message).filter(
                    models.Message.recipient_id == self.user.id)

        # FIXME: implement pagination
        return messages.all()

    def delete_many_by_id(self, messages):
        session = sessions.get_scoped_session()

        query = session.query(models.Message).filter(
            models.Message.recipient_id == self.user.id,
            models.Message.id.in_([x.lower() for x in messages]))

        if query.count() != len(messages):
            raise NotFoundException("ono or more blobs not found")

        query.delete(synchronize_session=False)

    @transaction_method
    def save(self, session, **kwargs):
        contact_service = ContactsService(self.user)
        users_service = UsersService()

        to, content, meta, subject = (kwargs['to'].lower(), kwargs['content'],
                                            kwargs['meta'], kwargs['subject'])
        contact = contact_service.get(to)
        if contact.is_local:
            recipient = users_service.get_by_id(contact.user_id)
            message = self.new(recipient=recipient, subject=subject,
                                content=content, meta=meta)

            if "reply_to" in kwargs:
                self.update_with_reply_to(message, kwargs['reply_to'],
                                                kwargs['content_self'])
