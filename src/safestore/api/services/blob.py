# -*- coding: utf-8 -*-

from collections import namedtuple

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from djalchemy.transaction import transaction_method

from safestore.storage import models
from safestore.utils.exceptions import ValidateException, NotFoundException


def create_blob_dict(blob):
    obj = {
        "id": blob.id,
        "title": blob.title,
        "content": blob.content,
        "meta": blob.meta,
        "created_at": blob.created_at.isoformat(),
        "parent_id": blob.parent_blob_id or None,
    }

    return obj


class BlobsService(object):
    def __init__(self, user):
        self.user = user

    @transaction_method
    def create_for_share(self, session, data, parent, contact):
        #receiver = namedtuple("owner", ["id"])(data['receiver_id'].lower())
        blob = self.create(data)

        blob.parent_blob_id = parent.id
        blob.parent_owner_id = parent.owner_id
        blob.accepted = False
        blob.owner_id = contact.user_id

        session.add(blob)
        return blob

    @transaction_method
    def create(self, session, data):
        blob = models.Blob()
        blob.title = data['title']
        blob.content = data['content']
        blob.meta = data['meta']
        blob.owner_id = self.user.id

        session.add(blob)
        return blob

    def all(self, pending=False):
        session = sessions.get_scoped_session()

        query = session.query(models.Blob).filter(
                        models.Blob.owner_id == self.user.id,
                        models.Blob.accepted == (not pending))\
                            .order_by(desc(models.Blob.created_at))
        return query.all()

    @transaction_method
    def delete_many_by_id(self, session, blobs):
        query = session.query(models.Blob).filter(
            models.Blob.owner_id == self.user.id,
            models.Blob.id.in_([x.lower() for x in blobs]))

        if query.count() != len(blobs):
            raise NotFoundException("ono or more blobs not found")

        query.delete(synchronize_session=False)

    def get_by_id(self, id):
        session = sessions.get_scoped_session()
        try:
            return session.query(models.Blob).filter(
                            models.Blob.id == id.lower(),
                            models.Blob.owner_id == self.user.id).one()
        except NoResultFound:
            raise NotFoundException("Blob not found")

    @transaction_method
    def update_by_id(self, session, id, data):
        blob = self.get_by_id(id)

        if "title" in data:
            blob.title = data['title']

        if "content" in data:
            blob.content = data['content']

        if "meta" in data:
            blob.meta = data['meta']

        if "accepted" in data:
            blob.accepted = data['accepted']

        session.add(blob)
        return blob

