# -*- coding: utf-8 -*-

import uuid

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql import exists
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from djalchemy.core import sessions
from djalchemy.transaction import transaction_method

from safestore.storage import models
from safestore.utils.exceptions import ValidateException, NotFoundException


def file_part_to_dict(part):
    return {
        "id": part.id,
        "order": part.order,
        "size": part.size,
        "url": default_storage.url(part.path),
        "created_at": part.created_at.isoformat(),
    }


def file_to_dict(file):
    return {"id": file.id}


def file_pointer_to_dict(file_pointer):
    fdict = {
        "id": file_pointer.id,
        "file_id": file_pointer.file_id,
        "name": file_pointer.name,
        "meta": file_pointer.meta,
    }

    return fdict


class FilesService(object):
    def __init__(self, user):
        self.user = user

    def all(self):
        session = sessions.get_scoped_session()
        query = session.query(models.FilePointer).filter(
                    models.FilePointer.owner_id == self.user.id)

        return query.all()

    def exists(self, id):
        session = sessions.get_scoped_session()
        query = session.query(models.FilePointer).filter(
                    models.FilePointer.owner_id == self.user.id,
                    models.FilePointer.id == id)

        if not session.query(exists(query.statement)).scalar():
            raise ValidateException("file pointer does not exists.")

    def parts_for_pointer(self, pointer):
        session = sessions.get_scoped_session()
        query = session.query(models.FilePart).filter(
                    models.FilePart.file_id == pointer.file_id)

        return query.all()

    @transaction_method
    def create_file(self, session, total_parts):
        file = models.File(total_parts=total_parts)
        session.add(file)
        return file

    def get_file_by_id(self, id):
        session = sessions.get_scoped_session()
        try:
            file = session.query(models.File).filter_by(id=id).one()
        except NoResultFound:
            raise NotFoundException("File not found")

        return file

    @transaction_method
    def set_file_to_finish_state(self, session, file):
        file.finished = True
        session.add(file)
        #session.query(models.File).filter_by(id=file)\
        #        .update({"finished": True}, synchronize_session=False)

    @transaction_method
    def create_file_pointer_for_file(self, session, file, name, meta):
        file_pointer = models.FilePointer(file_id=file.id, owner_id=self.user.id)
        file_pointer.name = name
        file_pointer.meta = meta

        session.add(file_pointer)
        return file_pointer

    @transaction_method
    def create_file_part(self, session, file, order, data):
        id = str(uuid.uuid1())
        path = "{0}/{1}.part".format(file.id, id)

        if default_storage.exists(path):
            raise ValidateException("file exists")

        path = default_storage.save(path, ContentFile(data))

        file_part = models.FilePart(file_id=file.id, order=order)
        file_part.size = len(data)
        file_part.path = path
        session.add(file_part)
        return file_part

