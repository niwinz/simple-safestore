# -*- coding: utf-8 -*-

from django.conf import settings

from safestore.utils.exceptions import ValidateException
from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.user import UsersService, user_to_dict
from safestore.api.validators.user import UserRegisterValidator


class User(generic.View):
    @decorators.validation_intercept
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(UserRegisterValidator)
    def post(self, request, **kwargs):
        service = UsersService()
        user = service.create(request.data)
        service.send_verification_mail(user, request.data["email"])
        return http.HttpCreated(to_json({'id': user.id}),
                            content_type="application/json")

    @decorators.user_required
    def get(self, request, **kwargs):
        data = to_json(user_to_dict(request.user))
        return http.HttpResponse(data, content_type="application/json")

    def head(self, request):
        if "username" not in request.GET:
            raise ValidateException("Wrong arguments")

        username = request.GET['username']
        service = UsersService()

        if service.exists_by_username(username):
            return http.HttpNoContent()
        return http.HttpBadRequest()

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    def patch(self, request, **kwargs):
        service = UsersService()
        service.update_by_id(request.user.id, request.data)
        return http.HttpNoContent()

    @decorators.user_required
    def delete(self, request, **kwargs):
        """
        API: delete all data of the user.

        TODO: implement as celery task.
        """

        service = UsersService()
        service.delete_by_id(request.user.id)
        return http.HttpNoContent()


class Profile(generic.View):
    def get_blob_quotas(self, user):
        return {
            "blob_count": user.blobs.count(),
            "blob_total": user.max_blobs,
        }

    def get_contacts_quotas(self, user):
        return {
            "contact_count": user.contacts.count(),
        }

    def get_node_info(self):
        return {
            "node_name": settings.SAFESTORE_DOMAIN,
            "trusted_nodes": [x for x in settings.SAFESTORE_TRUSTED_NODES.keys()],
        }

    @decorators.user_required
    def get(self, request):
        ctx = {"id": request.user.id}

        ctx.update(self.get_blob_quotas(request.user))
        ctx.update(self.get_contacts_quotas(request.user))
        ctx.update(self.get_node_info())
        return http.HttpResponse(to_json(ctx), content_type="application/json")


class Activation(generic.View):
    @decorators.validation_intercept
    @decorators.request_body_required
    @decorators.json_contenttype_required
    def post(self, request):
        if "token" not in request.data:
            return http.HttpBadRequest("Invalid parameters")

        service = UsersService()
        service.verify(request.data["token"].lower())
        return http.HttpNoContent()
