# -*- coding: utf-8 -*-

from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.conf import settings

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func
from djalchemy.core import sessions

from safestore.storage import models
from safestore.utils.views import generic, decorators
from safestore.utils.json import to_json
from safestore.utils import crypto as crypto_utils
from safestore.utils import http

import hashlib
import json
import uuid

from .user import user_to_dict


class Login(generic.View):
    @decorators.json_contenttype_required
    @decorators.request_body_required
    def post(self, request):
        body = request.body.decode('utf-8')
        body = json.loads(body)

        errors = self.validate(body)
        if errors:
            return http.HttpBadRequest(to_json({"errors": errors}),
                                            content_type="application/json")

        token = crypto_utils.generate_new_token()
        cache.set(token, self.user.id, settings.SAFESTORE_TOKEN_TIMEOUT)

        data = user_to_dict(self.user)
        data.update({"token": token})

        return http.HttpResponse(to_json(data), content_type="application/json")

    def validate(self, body):
        if "username" not in body:
            return ["missing username attr"]

        if "password" not in body:
            return ["missing password attr"]

        session = sessions.get_scoped_session()
        try:
            user = session.query(models.User).filter(
                    func.lower(models.User.username) == body["username"].lower()).one()
        except NoResultFound:
            return ["username '{0}' not exists".format(body['username'])]

        if not user.is_active:
            return ['user is not activated']

        if not user.check_password(body['password']):
            return ["current password is incorrect"]

        self.user = user
        return []

    @decorators.user_required
    def delete(self, request, **kwargs):
        cache.delete(request.user_token)
        return http.HttpNoContent()
