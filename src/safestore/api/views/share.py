# -*- coding: utf-8 -*-

from djalchemy.transaction import transactional_view

from safestore.utils.json import request_json_to_dict
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.blob import BlobsService
from safestore.api.services.user import UsersService
from safestore.api.services.contact import ContactsService
from safestore.api.validators.blob import BlobValidator, BlobShareValidator


class ShareMixin(object):
    def share_object(self, body):
        raise NotImplementedError

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(BlobValidator)
    @decorators.apply_validator(BlobShareValidator)
    def post(self, request, id):
        response = self.share_object(request.data, id)
        if response is not None:
            return response
        return http.HttpNoContent()


@transactional_view
class ShareBlob(ShareMixin, generic.View):
    def share_object(self, body, parent_id):
        blob_service = BlobsService(self.request.user)
        contact_service = ContactsService(self.request.user)

        contact = contact_service.get(body["receiver_id"].lower())
        parent_blob = blob_service.get_by_id(parent_id)

        blob_service.create_for_share(body, parent_blob, contact)
