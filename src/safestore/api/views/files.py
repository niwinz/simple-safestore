# -*- coding: utf-8 -*-

from djalchemy.core import sessions
from djalchemy.transaction import transactional_view

from sqlalchemy.sql import exists

from safestore.storage import models
from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils.exceptions import ValidateException
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.file import FilesService, file_pointer_to_dict
from safestore.api.services.file import file_part_to_dict, file_to_dict


@transactional_view
class Files(generic.View):
    def clean_create_request_args(self, request):
        """
        Method for clean arguments for create request.
        """

        body = request_json_to_dict(request)

        if "total_parts" not in body:
            raise ValidateException("total_parts is mandatory on create requests")

        if not isinstance(body["total_parts"], int):
            raise ValidateException("total_parts has invalid value")

        return {"total_parts": body["total_parts"], "body": body}

    def clean_finish_request_args(self, request):
        """
        Method for clean arguments for finish request.
        """

        body = request_json_to_dict(request)
        session = sessions.get_scoped_session()

        try:
            file = session.query(models.File).filter(
                        models.File.id == body["file_id"]).one()
        except (ValueError, KeyError):
            raise ValidateException("Invalid value for file_id")
        except NoResultFound:
            raise ValidateException("file with file_id={0} does "
                                    "not exists".format(body["file_id"]))
        if file.finished:
            raise ValidateException("file with file_id={0} is "
                                    "in finished state".format(body["file_id"]))
        if "name" not in body:
            raise ValidateException("name argument is mandatory")

        if "meta" not in body:
            raise ValidateException("meta argument is mandatory")

        return {"file": file, "name": body["name"],
                "meta": body["meta"], "body": body}

    def clean_continue_request_args(self, request):
        if "file_id" not in request.GET:
            raise ValidateException("file_id is mandatory on continue requests")

        file_id = request.GET["file_id"]
        session = sessions.get_scoped_session()
        try:
            file = session.query(models.File).filter(
                        models.File.id == file_id).one()
        except NoResultFound:
            raise ValidateException("file with file_id={0} does "
                                    "not exists".format(file_id))
        if file.finished:
            raise ValidateException("file with file_id={0} is "
                                    "in finished state".format(file_id))
        try:
            order = int(request.GET["order"])
        except (ValueError, KeyError) as e:
            raise ValidateException(str(e))

        query = session.query(models.FilePart).filter(
                    models.FilePart.file_id == file.id,
                    models.FilePart.order == order)

        if session.query(exists(query.statement)).scalar():
            raise ValidateException("FilePart with order={} "
                                    "exists".format(order))

        return {"file": file, "order": order, "body": request.body}

    def clean_request_args(self, request):
        """
        General method for clenaing request arguments.
        """

        if "cmd" not in request.GET:
            raise ValidateException("cmd argument ir mandatory")

        cmd = request.GET['cmd']
        if cmd not in ['create', 'continue', 'finish']:
            raise ValidateException("cmd argument is wrong")

        arguments = {"cmd": cmd}
        if cmd == "create":
            arguments.update(self.clean_create_request_args(request))
        elif cmd == "continue":
            arguments.update(self.clean_continue_request_args(request))
        elif cmd == "finish":
            arguments.update(self.clean_finish_request_args(request))
        return arguments

    @decorators.user_required
    @decorators.validation_intercept
    def post(self, request):
        """
        Entry point for upload files in chunks.
        """

        arguments = self.clean_request_args(request)
        result = getattr(self, "post_" + arguments['cmd'])(request, arguments)

        return http.HttpResponse(to_json(result),
                        content_type="application/json")

    @decorators.json_contenttype_required
    @decorators.request_body_required
    def post_create(self, request, arguments):
        service = FilesService(request.user)
        file = service.create_file(arguments["total_parts"])
        return file_to_dict(file)

    @decorators.request_body_required
    def post_continue(self, request, arguments):
        service = FilesService(request.user)

        file, order = arguments["file"], arguments["order"]
        part = service.create_file_part(arguments["file"],
                    arguments["order"],arguments["body"])

        return file_part_to_dict(part)

    @decorators.json_contenttype_required
    @decorators.request_body_required
    def post_finish(self, request, args):
        service = FilesService(request.user)
        pointer = service.create_file_pointer_for_file(args["file"],
                                            args["name"], args["meta"])
        return file_pointer_to_dict(pointer)
