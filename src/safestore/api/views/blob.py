# -*- coding: utf-8 -*-

from djalchemy.transaction import transactional_view

from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils.exceptions import ValidateException
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.blob import BlobsService, create_blob_dict
from safestore.api.validators.blob import BlobValidator


@transactional_view
class Blobs(generic.View):
    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(BlobValidator)
    def post(self, request):
        service =  BlobsService(request.user)
        blob = service.create(request.data)
        return http.HttpResponse(to_json(create_blob_dict(blob)),
                                            content_type="application/json")

    @decorators.user_required
    def get(self, request):
        pending = "pending" in request.GET or False
        service = BlobsService(request.user)
        data = [create_blob_dict(x) for x in service.all(pending=pending)]
        return http.HttpResponse(to_json(data), content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    def delete(self, request):
        body = request_json_to_dict(request)

        if "blobs" not in body:
            return http.HttpBadRequest();

        service = BlobsService(request.user)
        service.delete_many_by_id(body["blobs"])
        return http.HttpNoContent()


@transactional_view
class Blob(generic.View):
    @decorators.validation_intercept
    @decorators.user_required
    def get(self, request, id):
        service = BlobsService(request.user)
        blob = service.get_by_id(id.lower())
        return http.HttpResponse(to_json(create_blob_dict(blob)),
                                        content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    def patch(self, request, id):
        body = request_json_to_dict(request)
        service = BlobsService(request.user)
        blob = service.update_by_id(id.lower(), body)
        return http.HttpResponse(to_json(create_blob_dict(blob)),
                                            content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    def delete(self, request, id):
        service = BlobsService(request.user)
        service.delete_many_by_id([id.lower()])
        return http.HttpNoContent()
