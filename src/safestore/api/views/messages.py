# -*- coding: utf-8 -*-

from djalchemy.transaction import transactional_view

from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils.exceptions import ValidateException
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.messages import MessagesService, message_to_dict
from safestore.api.validators.messages import MessagesValidator


@transactional_view
class Messages(generic.View):
    @decorators.user_required
    def get(self, request):
        service = MessagesService(request.user)
        items = [message_to_dict(x) for x in service.all()]
        return http.HttpResponse(to_json(items), content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(MessagesValidator)
    def put(self, request, **kwargs):
        msg_service = MessagesService(request.user)
        msg_service.save(**request.data)
        return http.HttpNoContent()


@transactional_view
class Message(generic.View):
    @decorators.user_required
    @decorators.validation_intercept
    def delete(self, request, id):
        service = MessagesService(request.user)
        service.delete_many_by_id([id])
        return http.HttpNoContent()
