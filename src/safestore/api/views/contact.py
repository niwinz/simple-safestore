# -*- coding: utf-8 -*-

from djalchemy.transaction import transactional_view
from safestore.utils.json import to_json, request_json_to_dict
from safestore.utils.exceptions import ValidateException
from safestore.utils.views import generic, decorators
from safestore.utils import http

from safestore.api.services.contact import ContactsService, create_contact_dict
from safestore.api.validators.contact import ContactsValidator
from safestore.api.validators.contact import ContactsDeleteValidator


@transactional_view
class Contacts(generic.View):
    @decorators.user_required
    def get(self, request):
        service = ContactsService(request.user)
        data = [create_contact_dict(x) for x in service.all()]
        return http.HttpResponse(to_json(data),
                    content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(ContactsValidator)
    def post(self, request):
        service = ContactsService(request.user)
        contact = service.create(request.data)
        return http.HttpCreated(to_json(create_contact_dict(contact)),
                                            content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    @decorators.apply_validator(ContactsDeleteValidator)
    def delete(self, request):
        service = ContactsService(request.user)
        service.delete_many_by_id(request.data["contacts"])
        return http.HttpNoContent()


@transactional_view
class Contact(generic.View):
    @decorators.validation_intercept
    @decorators.user_required
    @decorators.request_body_required
    @decorators.json_contenttype_required
    def patch(self, request, id):
        service = ContactsService(request.user)
        contact = service.update_by_id(id.lower(), request.data)
        return http.HttpResponse(to_json(create_contact_dict(contact)),
                                        content_type="application/json")

    @decorators.validation_intercept
    @decorators.user_required
    def delete(self, request, id):
        service = ContactsService(request.user)
        service.delete_many_by_id([id.lower()])
        return http.HttpNoContent()
