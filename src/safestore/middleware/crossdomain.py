# -*- coding: utf-8 -*-


class CrossDomainMiddleware(object):
    def process_response(self, request, response):
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Headers"] = "origin, content-type, accept, x-safestore-token, x-requested-with, X_FILENAME"
        response["Access-Control-Allow-Methods"] = "PUT, POST, OPTIONS, GET, DELETE, PATCH"
        return response

