# -*- coding: utf-8 -*-

from django.core.cache import cache
from django.conf import settings

from djalchemy.core import sessions
from sqlalchemy.orm.exc import NoResultFound

from safestore.storage import models
from safestore.utils import http

SAFESTORE_TOKEN_HEADER = 'HTTP_X_SAFESTORE_TOKEN'


class AuthMiddleware(object):
    def process_request(self, request):
        request.user = None

        if SAFESTORE_TOKEN_HEADER in request.META:
            token = request.META[SAFESTORE_TOKEN_HEADER]
            user_id = cache.get(token)

            if not user_id:
                return

            s = sessions.get_scoped_session()
            try:
                request.user = s.query(models.User).filter(
                                models.User.id == user_id).one()

                request.user_token = token
                cache.set(token, user_id,
                    settings.SAFESTORE_TOKEN_TIMEOUT)
                return None
            except NoResultFound:
                return http.HttpUnauthorized("Unauthenticaded request.")
