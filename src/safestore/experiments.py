# -*- coding: utf-8 -*-

import hashlib

from django.conf.urls import patterns, include, url
from django.http import HttpResponse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from safestore.utils.views import generic
from safestore.utils import http


class BinaryUpload(generic.View):
    def get(self, request):
        return HttpResponse("Sample output")

    def post(self, request):
        if default_storage.exists("test.data"):
            default_storage.delete("test.data")

        path = default_storage.save("test.data", ContentFile(request.body))
        return HttpResponse(default_storage.url(path))


urlpatterns = patterns('',
    url(r'^binary-upload$', BinaryUpload.as_view()),
)
