#!/bin/sh
coverage run --source="./safestore/api,./safestore/storage,./safestore/middleware" runtests.py
coverage report -m
rm -rf .coverage
