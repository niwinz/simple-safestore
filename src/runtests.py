# -*- coding: utf-8 -*-

import unittest
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "safestore.settings.testing")

from django.db import models
from djalchemy.core import engines, sessions
from djalchemy.declarative import BaseModel


def set_up_test_environment():
    # Django
    from django.conf import settings
    from django.core import mail
    settings.EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
    settings.ALLOWED_HOSTS = ['*']
    mail.outbox = []

    # Database
    models.get_models()
    BaseModel.metadata.create_all(engines["default"])


def tear_down_test_environment():
    BaseModel.metadata.drop_all(engines["default"])


if __name__ == "__main__":
    set_up_test_environment()

    runner = unittest.TextTestRunner(verbosity=2, failfast=True)
    loader = unittest.TestLoader()

    args = sys.argv[1:]

    if args:
        tests = loader.loadTestsFromNames(args)
    else:
        tests = loader.discover(".")

    result = runner.run(tests)
    tear_down_test_environment()

    sys.exit(not result.wasSuccessful())
    #unittest.main(verbosity=2, failfast=True)
